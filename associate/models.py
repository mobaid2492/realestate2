from django.db import models
#from django.contrib.auth.models import User
from user.models import User
from django.utils import timezone
from mptt.models import MPTTModel, TreeForeignKey

class Associate(MPTTModel):
    GENDER = (             
        ('M', 'Male'),
        ('F', 'Female'),     
    )
    user=models.ForeignKey(User, on_delete=models.CASCADE,blank=True,null=True)
    title=models.CharField(max_length=5,blank=True, null=True,default='Mr')
    name= models.CharField(max_length=100)
    parent= TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    country_code=models.CharField(max_length=50,blank=True,null=True)   
    contact_no=models.BigIntegerField(unique=False)
    alternate_no=models.BigIntegerField(null=True, blank=True)
    email=models.EmailField(unique=False)
    address=models.CharField(max_length=100,null=True, blank=True)
    pincode=models.IntegerField(null=True, blank=True,)
    photo=models.ImageField(upload_to='images/associate/',blank=True, null=True)
    gender=models.CharField(max_length=1, choices=GENDER)
    status=models.BooleanField(default=True,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)
    date_of_birth=models.DateTimeField(auto_now=False, auto_now_add=False,null=True, blank=True)
    pan_no=models.CharField(max_length=10,blank=True, null=True)
    bank_name=models.CharField(max_length=50,blank=True, null=True)
    branch_name=models.CharField(max_length=50,blank=True, null=True)
    account_no=models.CharField(max_length=50,blank=True, null=True)
    ifsc_code=models.CharField(max_length=50,blank=True, null=True)
    is_payment_on_hold=models.BooleanField(default=False)
    class MPTTMeta:
        order_insertion_by = ['name']
# Create your models here.
