from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth, messages
from django.utils import timezone
from projects.models import Project, Section, Property, PriceRule, EMI, Transaction
from projects.forms import ProjectForm, SectionForm, PropertyForm
from .models import Associate
from .forms import AssociateForm, UpdateForm, DownlineForm
from customers.models import Customer
from reports.models import MonthlySales, Sale, PayoutLifetime, PayoutMonthly, PayoutRule, SelfEarning, TeamEarning, AssociateTransaction
from django.db.models import Sum, Count, Max
from user.views import passwordChange
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from reports.tasks import *
# side menu options
url = "/associate/"


menu = [
    {"id": "1",
     "name": "Dashboard",
     "url": url+"",
     "icon": "fa-tachometer-alt",
     "active": "",
     "sub": [

     ],
     },
    {"id": "2",
     "name": "Projects",
     "url": url+"project",
     "icon": "fa-landmark",
     "active": "",
     "sub": [
         {"name": "View Projects",
          "url": url+"project", },
         {"name": "PLC Rules",
             "url": url+"project/plc", },

     ],
     },
    {"id": "3",
     "name": "My Business",
     "url": url+"project",
     "icon": "fa-briefcase",
     "active": "",
     "sub": [
         {"name": "View my Clients",
          "url": url+"customers", },
         {"name": "My Buisness Summary",
             "url": url+"buisness-summary", },
         {"name": "Team Buisness",
             "url": url+"team-buisness", },
         {"name": "My Buisness Rewards",
             "url": url+"rewards", },

     ],
     },
    {"id": "4",
        "name": "My team",
        "url": url+"project",
        "icon": "fa-users",
        "active": "",
        "sub": [

            {"name": "My Uplines",
             "url": url+"upline", },

            {"name": "My Downlines",
             "url": url+"downlines", },
            {"name": "Add New Downline",
             "url": url+"downlines/add", },

        ],
     },
    {"id": "5",
        "name": "Finance",
        "url": url+"project",
        "icon": "fa-rupee-sign",
        "active": "",
        "sub": [

            {"name": "My Earnings",
             "url": url+"my-earnings", },
            {"name": "Earning Forcast",
             "url": url+"earnings-forcast", },
            {"name": "Passbook",
             "url": url+"passbook", },

        ],
     },
]
# assoicate_id=user


def sidebarActive(id):
    for menuitem in menu:
        if menuitem['id'] == id:
            menuitem['active'] = "active"
        else:
            menuitem['active'] = ""


def index(request):
    sidebarActive("1")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    monthly_report = Sale.objects.values('createdAt', 'amount').filter(associate_ref=associate).filter(status=True)
    lifetime_sales = Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
    team_monthly_report = MonthlySales.objects.values('sale_month', 'amount').filter(associate_ref__parent=associate, createdAt__year=2019)
    payout_self = PayoutLifetime.objects.only('payout_rule').filter(associate_ref=associate).order_by('-createdAt')[:1]
    payout_self_monthly = PayoutMonthly.objects.only('payout_rule').filter(associate_ref=associate).order_by('-createdAt')[:1]
    context = {

        "menu": menu,
        "associate": associate,
        "monthly_report": monthly_report,
        "team_monthly_report": team_monthly_report,
        "lifetime_sales": lifetime_sales,
        "payout_self": payout_self,
        "payout_self_monthly": payout_self_monthly,
        "title":"Dashboard"

    }
    return render(request, 'associate/buisness/buisnessSummary.html', context)


#######################################################################################################################
# PROJECTS                                           Projects                                              Projects
#######################################################################################################################

def plcRules(request):
    sidebarActive("2")
    rules = PriceRule.objects.all()
    context = {
        "menu": menu,
        "rules": rules,
    }
    return render(request, 'projects/admin_plc_rules.html', context)
def projects(request):
    sidebarActive("2")
    project_list = Project.objects.all()
    form = ProjectForm(request.POST or None)
    user = request.user
    context = {
        "projects": project_list,
        "form": form,
        "menu": menu,
        "user": request.user
    }
    return render(request, 'projects/admin_projects.html', context)
def projectView(request, id):
    sidebarActive("2")
    project = Project.objects.get(id=id)
    sections = Section.objects.all().filter(project_ref=id)
    properties = Property.objects.all()
    form = SectionForm(request.POST or None)
    user = request.user
    context = {
        "project": project,
        "menu": menu,
        "form": form,
        "sections": sections,
        "properties": properties,
        "user": user
    }
    return render(request, 'projects/admin_projectView.html', context)
def sectionView(request, proid, id):
    sidebarActive("2")
    project = Project.objects.get(id=proid)
    sections = Section.objects.get(id=id)
    properties = Property.objects.all().filter(section_ref=id)
    price_rules = PriceRule.objects.all()
    form = PropertyForm(request.POST or None)
    user = request.user
    context = {
        "project": project,
        "menu": menu,
        "form": form,
        "section": sections,
        "properties": properties,
        "price_rules": price_rules,
    }
    return render(request, 'projects/admin_sectionView.html', context)

def propertyView(request, id):
    sidebarActive("2")
    properties = Property.objects.get(id=id)
    context = {
        "property": properties,
        "menu": menu,
    }
    return render(request, 'projects/admin_propertyView.html', context)


#######################################################################################################################
# Team                                                  Team                                              Team
#######################################################################################################################


def uplineView(request):
    sidebarActive("4")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    context = {
        "menu": menu,
        "associate": associate,
    }
    return render(request, 'associate/uplineView.html', context)


def downlines(request):
    sidebarActive("4")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    associate_list = Associate.objects.all().filter(parent=associate)
    customers = Customer.objects.all()
    context = {

        "menu": menu,
        "associate": associate,
        "associate_list": associate_list,
        "customers": customers,
    }
    return render(request, 'associate/associateList.html', context)

def downlineView(request, id):
    sidebarActive("4")    
    associate = Associate.objects.get(id=id)
    #sales=Sale.objects.select_related('property_ref').filter(associate_ref_id=id)
    downline_count = Associate.objects.all().filter(parent=id).aggregate(Count('id'))
    customers_list = Customer.objects.all().filter(associate_ref=id)
    properties = Property.objects.all()
    #ssociateTask.createTree.delay(id)
    #AssociateTask.get_downlineTree.delay(id)
    sales=AssociateTask.getDownlineSalesTree(id)
    context = {

        "menu": menu,
        "associate": associate,
        "downline_count": downline_count,
        "customers_list": customers_list,
        "properties": properties,
        "sales":sales,
       

    }
    return render(request, 'associate/associateView.html', context)


def downlineCreateView(request):
    sidebarActive("4")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    form = AssociateForm(request.POST, request.FILES or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        photo = form.cleaned_data['photo']
        password = form.cleaned_data['password']
        uplineID = form.cleaned_data['uplineID']
        contact_no = form.cleaned_data['contact_no']
        alternate_no = form.cleaned_data['alternate_no']
        address = form.cleaned_data['address']
        pincode = form.cleaned_data['pincode']
        gender = form.cleaned_data['gender']
        date_of_birth = form.cleaned_data['date_of_birth']
        pan_no = form.cleaned_data['pan_no']
        bank_name = form.cleaned_data['bank_name']
        account_no = form.cleaned_data['account_no']
        ifsc_code = form.cleaned_data['ifsc_code']
        name = first_name+" "+last_name
        try:
            upline = Associate.objects.get(user__username=uplineID)
        except Associate.DoesNotExist:
            messages.error(request, 'Wrong upline ID')
        try:
            associate = Associate.objects.get(
                contact_no=form.cleaned_data['contact_no'])
            #associate = Associate.objects.get(email=form.cleaned_data['email'])
            messages.error(
                request, 'The requested Associate is already registered')
        except Associate.DoesNotExist:
            newuser = AssociateTask.create_user_accounts(
                email, first_name, last_name, photo, 7, password)
            #newassociate.save()
            messages.success(request, 'The Associate has been Saved')
            Associate.objects.create(user=newuser, name=name, parent=associate, country_code='+91',
                                     contact_no=contact_no, alternate_no=alternate_no, email=email,
                                     address=address, pincode=pincode, photo=photo, gender=gender,
                                     date_of_birth=date_of_birth, pan_no=pan_no, bank_name=bank_name,
                                     account_no=account_no, ifsc_code=ifsc_code)
            return redirect('associate:associates')
    elif request.POST:
        messages.error(request, 'Incorrect Form Details')    #

    context = {
        "menu": menu,
        "form": form,

    }
    return render(request, 'associate/associateCreate.html', context)
#######################################################################################################################
#Buisness                                             Buisness                                              Buisness
#######################################################################################################################
def customerListView(request):
    sidebarActive("3")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    customers = Customer.objects.filter(associate_ref=associate)
    properties = Property.objects.all()
    context = {
        "customer_list": customers,
        "menu": menu,

        "properties": properties

    }
    return render(request, 'customers/admin_customerList.html', context)


def customerView(request, id):
    sidebarActive("3")
    customer = Customer.objects.get(id=id)
    properties = Property.objects.all().filter(customer_ref=id)
    emi = EMI.objects.all().filter(property_ref__customer_ref=id)
    tx = Transaction.objects.all().filter(property_ref__customer_ref=id)
    paid_emi = EMI.objects.all().filter(property_ref__customer_ref=id).filter(
        status=True).aggregate(Count('emi'))
    pending_emi = EMI.objects.all().filter(property_ref__customer_ref=id).filter(
        status=False).aggregate(Count('emi'))
    context = {
        "customer": customer,
        "menu": menu,
        "properties": properties,
        "amount": "amount",
        "emi": emi,
        "pending_emi": pending_emi,
        "paid_emi": paid_emi,
        "tx": tx,
    }
    return render(request, 'customers/admin_customerView.html', context)

def buisnessSummary(request):
    sidebarActive("3")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    monthly_report = Sale.objects.values('createdAt', 'amount').filter(associate_ref=associate).filter(status=True)
    lifetime_sales = Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
    team_sales_report = AssociateTask.getDownlineSalesTree(associate.id) #.objects.values('sale_month', 'amount').filter(associate_ref__parent=associate, createdAt__year=2019)
    payout_self = PayoutLifetime.objects.only('payout_rule').filter(associate_ref=associate).order_by('-createdAt')[:1]
    payout_self_monthly = PayoutMonthly.objects.only('payout_rule').filter(associate_ref=associate).order_by('-createdAt')[:1]
    context = {

        "menu": menu,
        "associate": associate,
        "monthly_report": monthly_report,
        "team_monthly_report": team_sales_report,
        "lifetime_sales": lifetime_sales,
        "payout_self": payout_self,
        "payout_self_monthly": payout_self_monthly

    }
    return render(request, 'associate/buisness/buisnessSummary.html', context)


def teamBuisness(request):
    sidebarActive("3")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    lifetime_sales = Sale.objects.filter(associate_ref=associate).aggregate(Sum('amount'))
    team_monthly_report = Sale.objects.filter(associate_ref__parent=associate)
    downlines = Associate.objects.values('id', 'name').filter(parent=associate)
    payout_lifetime = PayoutLifetime.objects.select_related('payout_rule').only('associate_ref', 'payout_rule').filter(associate_ref__parent=associate)
    payout_self = PayoutLifetime.objects.only('payout_rule').filter(associate_ref=associate).order_by('payout_rule')[:1]
    payout_self_monthly = PayoutMonthly.objects.only('payout_rule').filter( associate_ref=associate).order_by('-createdAt')[:1]
    payout_monthly = PayoutMonthly.objects.select_related('payout_rule').values('associate_ref', 'payout_rule', 'createdAt').filter(associate_ref__parent=associate)

    context = {

        "menu": menu,
        "associate": associate,
        "downlines": downlines,
        "team_monthly_report": team_monthly_report,
        "payout_monthly": payout_monthly,
        "payout_lifetime": payout_lifetime,
        'lifetime_sales': lifetime_sales,
        'payout_self': payout_self


    }
    return render(request, 'associate/buisness/teambuisness.html', context)


def leaderRewards(request):
    sidebarActive("3")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    context = {
        "menu": menu,
        "associate": associate,
    }
    return render(request, 'associate/buisness/leaderRewards.html', context)


def rewardView(request):
    sidebarActive("3")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')    
    transactions=RewardPassbook.objects.filter(associate_ref=associate)
    rewards=RewardEarning.objects.select_related('reward_rule').filter(associate_ref=associate)
    reward_passbook=RewardPassbook.objects.only('sale_amount_balance','reward_balance').filter(associate_ref=associate).order_by('-createdAt')[:1].aggregate(Sum('sale_amount_balance'))
    reward_rule=RewardsRule.objects.all() 
   
    context = {
        "menu": menu,   
        "associate":associate,
        "rewards":rewards,
        "transactions":transactions,
        "reward_rule":reward_rule,
        "reward_passbook":reward_passbook,
         

    }
    return render(request, 'owner/finance/redeem_rewards.html', context)
#######################################################################################################################
# Finance                                                Finance                                              Finance
#######################################################################################################################


def myEarnings(request):
    sidebarActive("5")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    payout_self = PayoutLifetime.objects.only('payout_rule').filter(
        associate_ref=associate).order_by('payout_rule')[:1]
    earnings = SelfEarning.objects.all().filter(
        associate_ref=associate).order_by('createdAt').exclude(is_onhold=True)
    team_earnings = TeamEarning.objects.all().filter(
        associate_ref=associate).order_by('createdAt')
    # generate_monthly_sales_report()
    # month=update_lifetime_payout(associate)
    context = {

        "menu": menu,
        "associate": associate,
        "year": "month",
        "earnings": earnings,
        "team_earnings": team_earnings
    }
    return render(request, 'associate/finance/myEarnings.html', context)


def earningForcast(request):
    sidebarActive("5")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
        # property_ref__cust_id__associate_ref=associate
    forcast = EMI.objects.only('pay_day', 'emi').filter(property_ref__customer_ref__associate_ref=associate)
    context = {

        "menu": menu,
        "associate": associate,
        "forcast": forcast
    }
    return render(request, 'associate/finance/earningsForcast.html', context)


def passbook(request):
    sidebarActive("5")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
    associate_tx = AssociateTransaction.objects.filter(associate_ref=associate)
    context = {

        "menu": menu,
        "associate": associate,
        "associate_tx": associate_tx
    }
    return render(request, 'associate/finance/passbook.html', context)

##################### Profile ###############################################


def myProfile(request):
    sidebarActive("0")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('user:login')
   
    #sales=Sale.objects.select_related('property_ref').filter(associate_ref_id=id)
    downline_count = Associate.objects.all().filter(parent=associate).aggregate(Count('id'))
    customers_list = Customer.objects.all().filter(associate_ref=associate)
    properties = Property.objects.all()
    downlines= Associate.objects.all().filter(parent=associate)
  
    #ssociateTask.createTree.delay(id)
    #AssociateTask.get_downlineTree.delay(id)
    sales=AssociateTask.getDownlineSalesTree(associate.id)
    context = {

        "menu": menu,
        "associate": associate,
        "downline_count": downline_count,
        "customers_list": customers_list,
        "properties": properties,
        "sales":sales,
        "downlines":downlines,
       

    }
    return render(request, 'associate/associateView.html', context)


def profileEdit(request):
    sidebarActive("0")
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('associate:profile_edit')

    form = UpdateForm(request.POST, request.FILES, instance=associate)
    if form.is_valid():
        form.save()
        messages.success(
            request, 'Your profile was updated successfully updated!')
        return redirect(reverse('associate:profile'))
    context = {

        "menu": menu,
        "associate": associate,
        "form": form,

    }
    return render(request, 'associate/profileEdit.html', context)


def changePwd(request):
    current_user = request.user
    try:
        associate = Associate.objects.get(user=current_user)
    except:
        return redirect('associate:changepwd')
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(
                request, 'Your password was successfully updated!')
            return redirect('associate:home')
        else:
            messages.error(request, 'Passwords do not match')
    else:
        form = PasswordChangeForm(request.user)
    context = {
        "menu": menu,
        "associate": associate,
        "form": form,
    }
    return render(request, 'user/changePassword.html', context)
