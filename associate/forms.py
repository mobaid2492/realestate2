from django import forms
from .models import Associate
class AssociateForm(forms.Form):
    GENDER = (             
        ('M', 'Male'),
        ('F', 'Female'),   
    )    
    title=forms.CharField(max_length=5)
    first_name= forms.CharField(max_length=100)
    last_name= forms.CharField(max_length=100)
    password= forms.CharField(max_length=50)
    photo=forms.ImageField()
    uplineID=forms.CharField(max_length=10, required=False)
    contact_no=forms.IntegerField(required=True)
    alternate_no=forms.IntegerField()
    email= forms.EmailField(required=True)
    address=forms.CharField(max_length=100)
    pincode=forms.IntegerField()
    gender=forms.ChoiceField(choices=GENDER)
    date_of_birth=forms.DateField(required=False)
    pan_no=forms.CharField(max_length=10)
    branch_name=forms.CharField(max_length=50)
    bank_name=forms.CharField(max_length=50)
    account_no=forms.CharField(max_length=50)
    ifsc_code=forms.CharField(max_length=50)
class AssociateEditForm(forms.Form):
    GENDER = (             
        ('M', 'Male'),
        ('F', 'Female'),   
    )    
    title=forms.CharField(max_length=5)
    first_name= forms.CharField(max_length=100)
    last_name= forms.CharField(max_length=100)
    photo=forms.ImageField(required=False)
    uplineID=forms.CharField(max_length=10, required=False)
    contact_no=forms.IntegerField(required=True)
    alternate_no=forms.IntegerField()
    email= forms.EmailField(required=True)
    address=forms.CharField(max_length=100)
    pincode=forms.IntegerField()
    gender=forms.ChoiceField(choices=GENDER)
    date_of_birth=forms.DateField(required=False)
    pan_no=forms.CharField(max_length=10)
    branch_name=forms.CharField(max_length=50)
    bank_name=forms.CharField(max_length=50)
    account_no=forms.CharField(max_length=50)
    ifsc_code=forms.CharField(max_length=50)

class DownlineForm(forms.ModelForm):
    class Meta:
        model= Associate
        fields= '__all__'
class UpdateForm(forms.ModelForm):
    class Meta:
        model= Associate
        fields= [
        'name',        
        'contact_no',
        'alternate_no',
        'email',
        'address',
        'photo'
        ]


