from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import Group
from django.contrib import auth,messages
from config.models import Company
from .models import User
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from .forms import UserForm,NewUserForm
from django.utils.crypto import get_random_string

from user.menu import Menu

def getCompany():
  company_detail=Company.objects.all()
  return company_detail


def general_access(request):
  user=request.user
  if user.user_type == 7:
    messages.danger(request,'Access Denied!!')
   
def index(request):
  context={
           
           "details":getCompany(),                                 
    }
  return render(request ,'user/index.html', context)
def login(request):
    context={
           
           "details":getCompany(),                                 
    }
    if request.method=='POST':
        user= auth.authenticate(username=request.POST['username'],password=request.POST['password'])
        if user is not None:
            auth.login(request,user)
            if user.user_type == 7:
              return redirect('associate:home')
            elif user.user_type is not None:
              return redirect('owner:home')
            else:
              print(user.groups.name)
              return redirect('webapp:home')
           # return redirect('associate:home')
        else:
            context={
           
           "details":getCompany(),
           'error':' Wrong ID or password '                                 
            }
            return render(request ,'user/login.html', context) 
    else:
        return render(request ,'user/login.html', context)


def register(request):
  context={
           
           "details":getCompany(),                                 
    }
  if request.method=='POST':
        try:
            if request.POST['password']==request.POST['cnf-password']:
                user = User.objects.get(username=request.POST['username'])
                context1={
           
                  "details":getCompany(),
                  'error':'Oops! This email is already registered'                                 
                    }
                return render(request ,'user/register.html', context1) 
        except :
           
            user= User.objects.create_user(request.POST['username'],password=request.POST['password'], email=request.POST['username'])
            group = Group.objects.get(name='Associate')
            user.groups.add(group)
            auth.login(request,user)
            if  user.groups.filter(name='Associate').exists():
              return redirect('associate:home')
            else:
              print(user.groups.name)
              return redirect('webapp:home')
  else:
        return render(request ,'user/register.html', context)   
  return render(request ,'user/register.html', context)
  
def forgotPassword(request):
  
  menu=Menu.getMenu(request.user.user_type)
  context={
           
           "details":getCompany(),                                 
    }
  return render(request ,'user/index.html', context)
def logout(request):
    if request.method=='POST':
        auth.logout(request)
        return redirect('webapp:home')

def passwordChange(request):
      current_user = request.user
      menu=Menu.getMenu(request.user.user_type)       
      if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('user:login')
        else:
            messages.error(request, 'Passwords do not match')
      else:
        form = PasswordChangeForm(request.user)
      context={              
              "menu": menu,  
              "form":form,                       
       }
      return render(request, 'user/changePassword.html',context)
  
def profileEdit(request):
    current_user = request.user
    menu=Menu.getMenu(request.user.user_type)
    form=UserForm(request.POST or None,instance=current_user)
    if form.is_valid():
      form.save()
    context={              
              "menu": menu,  
              "form":form,                       
       }
    return render(request, 'user/profileEdit.html',context)
def profileView(request):
    current_user = request.user
    menu=Menu.getMenu(request.user.user_type)
    context={              
              "menu": menu,  
              "user": request.user,                       
       }
    return render(request, 'user/profile.html',context)
def userList(request):
    current_user = request.user
    menu=Menu.getMenu(request.user.user_type)
    user_list=User.objects.exclude(user_type=7).exclude(username="codinfy")
    context={              
              "menu": menu,  
              "user": request.user,
              "user_list":user_list                       
       }
    return render(request, 'user/userList.html',context)
def addUser(request):    
    menu=Menu.getMenu(request.user.user_type)
    form=NewUserForm(request.POST or None,request.FILES) 
    if form.is_valid() and request.POST:
        first_name=form.cleaned_data['first_name']
        last_name=form.cleaned_data['last_name']
        email=form.cleaned_data['email']
        user_type=form.cleaned_data['user_type']
        photo=form.cleaned_data['photo']
        username = 'CBDS{}'.format(get_random_string(4, '0123456789'))
        User.objects.create_user(username=username, email=email, password=username,
                                        photo=photo, first_name=first_name, last_name=last_name, user_type=(user_type))
        form=NewUserForm(request.POST or None,request.FILES)
        messages.success(request,'User has been created successfully!!')
        return redirect('user:user_list')
    context={              
              "menu": menu,  
              "form":form                       
       }
    return render(request, 'user/addUser.html',context)

def userDeactivate(request, id):
    current_user=request.user
    user =User.objects.get(id=id)  
    if current_user.user_type == 1 and current_user != user:
        user.is_active = False
        user.save()
        messages.success(request, 'User has been Deactivated Successfully')
    else:
      if current_user == user:
        messages.error(request, 'Access Denied: You are cannot deactivate yourself!')
      else:        
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('user:user_list')


def userActivate(request, id):
    current_user=request.user
    user =User.objects.get(id=id)  
    if current_user.user_type == 1 :
        user.is_active = True
        user.save()
        messages.success(request, 'User has been Activated Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('user:user_list')
