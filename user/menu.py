

class Menu():
    def getMenu(usertype):
        menu=[]
        if usertype == 1:
            menu=Menu.directorMenu()
        elif usertype == 2:
            menu=Menu.directorMenu()
        elif usertype == 3:
            menu=Menu.directorMenu()
        elif usertype == 4:
            menu=Menu.directorMenu()
        elif usertype == 7:
            menu=Menu.associateMenu()
        return menu
    def directorMenu():
        url = "/owner/"
        menu = [
        {"id": "1",
        "name": "Dashboard",
        "url": url+"",
        "icon": "fa-tachometer-alt",
        "active": "",
        "sub": [

        ],
        },
        {"id": "2",
        "name": "Projects",
        "url": url+"project",
        "icon": "fa-landmark",
        "active": "",
        "sub": [
            {"name": "View Projects",
            "url": url+"project", },
            {"name": "PLC Rules",
                "url": url+"project/plc", },

        ],
        },
        {"id": "3",
        "name": "Customers",
        "url": url+"customers",
        "icon": "fa-user",
        "active": "",
        "sub": [
            {"name": "View all Customers",
            "url": url+"customers"
            },
            {"name": "Add new Customers",
            "url": url+"customers/add"
            },


        ], },
        {"id": "4",
        "name": "Assocates",
        "url": url+"associates",
        "icon": "fa-users",
        "active": "",
        "sub": [{
            "name": "All Associates",
            "url": url+"associates",
        },
            {
            "name": "Add new Associate",
            "url": url+"associates/add",
        },


        ], },
        {"id": "5",
        "name": "Buisness",
        "url": url+"sales/pos/3",
        "icon": "fa-briefcase",
        "active": "",
        "sub": [
            {"name": "New Booking",
            "url": url+"sales/pos/", },
            {"name": "Final Bookings",
                "url": url+"sales/salesbook", },

        ], },
        {"id": "6",
        "name": "Finance       ",
        "url": url+"finance/login-amount",
        "icon": "fa-rupee-sign",
        "active": "",
        "sub": [
            {"name": "Login Amount",
                "url": url+"finance/login-amount", },
            {"name": "Payouts",
                "url": url+"finance/payouts", },
            {"name": "Rewards",
                "url": url+"finance/rewards", },
            """ {"name": "Refunds",
                "url": url+"finance/login-amount", }, """

        ], },
        {"id": "7",
        "name": "Reports",
        "url": url+"sales/pos/3",
        "icon": "fa-file-invoice-dollar",
        "active": "",
        "sub": [
            {"name": "Sales Report",
            "url": url+"reports/sales-report", },
            {"name": "Buisness Report",
            "url": url+"reports/buisness-report", },
            {"name": "Emi Report",
            "url": url+"reports/emi-report", },

        ], },
        {"id": "8",
        "name": "User Management",
        "url": url+"sales/pos/3",
        "icon": "fa-user-lock",
        "active": "",
        "sub": [ 
             {"name": "View All Users",
          "url": "/user/user-list", },
        {"name": "Add New Users",
          "url": "/user/add-user", },


        ], },
        {"id": "9",
        "name": "Company Policy",
        "url": url+"",
        "icon": "fa-file-signature",
        "active": "",
        "sub": [
            {"name": "Payout Rules",
            "url": url+"policy/payout/", },
            {"name": "Reward Rules",
            "url": url+"policy/rewards/", },
            

        ], }
    ]
        return menu

    def associateMenu():
        url = "/associate/"
        menu = [
            {"id": "1",
            "name": "Dashboard",
            "url": url+"",
            "icon": "fa-tachometer-alt",
            "active": "",
            "sub": [

            ],
            },
            {"id": "2",
            "name": "Projects",
            "url": url+"project",
            "icon": "fa-landmark",
            "active": "",
            "sub": [
                {"name": "View Projects",
                "url": url+"project", },
                {"name": "PLC Rules",
                    "url": url+"project/plc", },

            ],
            },
            {"id": "3",
            "name": "My Business",
            "url": url+"project",
            "icon": "fa-briefcase",
            "active": "",
            "sub": [
                {"name": "View my Clients",
                "url": url+"customers", },
                {"name": "My Buisness Summary",
                    "url": url+"buisness-summary", },
                {"name": "Team Buisness",
                    "url": url+"team-buisness", },
                {"name": "My Buisness Rewards",
                    "url": url+"rewards", },

            ],
            },
            {"id": "4",
                "name": "My team",
                "url": url+"project",
                "icon": "fa-users",
                "active": "",
                "sub": [

                    {"name": "My Uplines",
                    "url": url+"upline", },

                    {"name": "My Downlines",
                    "url": url+"downlines", },
                    {"name": "Add New Downline",
                    "url": url+"downlines/add", },

                ],
            },
            {"id": "5",
                "name": "Finance",
                "url": url+"project",
                "icon": "fa-rupee-sign",
                "active": "",
                "sub": [

                    {"name": "My Earnings",
                    "url": url+"my-earnings", },
                    {"name": "Earning Forcast",
                    "url": url+"earnings-forcast", },
                    {"name": "Passbook",
                    "url": url+"passbook", },

                ],
            },
        ]
        return menu

