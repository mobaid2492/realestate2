from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    USER_TYPE_CHOICES = (
      (1, 'director'),
      (2, 'moderator'),
      (3, 'supervisor'),
      (4, 'hr'),
      (5, 'sales'),
      (6, 'accountant'),
      (7, 'associate'),
  
      
  )
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES,blank=True, null=True)
    photo=models.ImageField(upload_to='images/user/',blank=True, null=True)


# Create your models here.
