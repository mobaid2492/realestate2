from django.urls import path,include
from . import views

app_name="user"
urlpatterns = [  
    
    path('login', views.login ,name='login'),
   # path('register', views.register ,name='register'),
   # path('forgot-password', views.forgotPassword ,name='forgot'),
    path('logout', views.logout ,name='logout'),
    path('password-change',views.passwordChange ,name='passwordChange'),
    path('profile-edit',views.profileEdit ,name='profile_edit'),
    path('profile-view',views.profileView ,name='profile_view'),
    path('user-list',views.userList,name='user_list'),
    path('add-user',views.addUser,name='add_user'),
    path('activate-user/<int:id>',views.userActivate,name='activate_user'),
    path('deactivate-user/<int:id>',views.userDeactivate,name='deactivate_user'),

     
    ]

    #project/id/edit
    