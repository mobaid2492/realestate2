from django.contrib.auth.models import User, Group
from rest_framework import serializers
from customers.models import Customer
from config.models import Company


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields= ('id','name','contact_no','alternate_no','email','address')

class ConfigSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields= ('id','name','is_active')
