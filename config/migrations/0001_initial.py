# Generated by Django 2.2.2 on 2019-08-06 20:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('legalName', models.CharField(max_length=50)),
                ('cin_no', models.CharField(blank=True, max_length=50, null=True)),
                ('fb_id', models.URLField(blank=True, null=True)),
                ('twitter_id', models.URLField(blank=True, null=True)),
                ('insta_id', models.URLField(blank=True, null=True)),
                ('short_desc', models.CharField(blank=True, max_length=300, null=True)),
                ('long_desc', models.TextField(blank=True, null=True)),
                ('logo', models.ImageField(blank=True, null=True, upload_to='images/config/')),
                ('geo_code_lat', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('geo_code_long', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('bg_url', models.URLField(blank=True, null=True)),
                ('bg_upload', models.ImageField(blank=True, null=True, upload_to='images/config/')),
                ('use_url', models.BooleanField(blank=True, null=True)),
                ('contact_no', models.CharField(max_length=50)),
                ('alternate_no', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'Company',
                'verbose_name_plural': 'Companies',
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('ref_no', models.CharField(blank=True, max_length=50, null=True)),
                ('bg_upload', models.FileField(blank=True, null=True, upload_to='files/config/docs/')),
            ],
            options={
                'verbose_name': 'Document',
                'verbose_name_plural': 'Documents',
            },
        ),
        migrations.CreateModel(
            name='Spotlight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('short_desc', models.CharField(blank=True, max_length=300, null=True)),
                ('bg_url', models.URLField(blank=True, null=True)),
                ('bg_upload', models.ImageField(blank=True, null=True, upload_to='images/config/')),
                ('use_url', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Spotlight',
                'verbose_name_plural': 'Spotlights',
            },
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('address', models.TextField(blank=True, null=True)),
                ('city', models.CharField(blank=True, max_length=50, null=True)),
                ('state', models.CharField(blank=True, max_length=50, null=True)),
                ('country', models.CharField(blank=True, max_length=50, null=True)),
                ('pincode', models.IntegerField(blank=True, null=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='config.Company')),
            ],
            options={
                'verbose_name': 'Office',
                'verbose_name_plural': 'Offices',
            },
        ),
        migrations.CreateModel(
            name='MediaMentions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('short_desc', models.CharField(blank=True, max_length=300, null=True)),
                ('bg_url', models.URLField(blank=True, null=True)),
                ('bg_upload', models.ImageField(blank=True, null=True, upload_to='images/config/')),
                ('use_url', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=False)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='config.Company')),
            ],
            options={
                'verbose_name': 'MediaMentions',
                'verbose_name_plural': 'MediaMentionss',
            },
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('short_desc', models.CharField(blank=True, max_length=300, null=True)),
                ('bg_url', models.URLField(blank=True, null=True)),
                ('bg_upload', models.ImageField(blank=True, null=True, upload_to='images/config/')),
                ('use_url', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=False)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='config.Company')),
            ],
            options={
                'verbose_name': 'Gallery',
                'verbose_name_plural': 'Gallerys',
            },
        ),
        migrations.CreateModel(
            name='BankDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_name', models.CharField(blank=True, max_length=50, null=True)),
                ('bank_address', models.TextField(blank=True, null=True)),
                ('account_name', models.CharField(blank=True, max_length=50, null=True)),
                ('account_number', models.CharField(blank=True, max_length=50, null=True)),
                ('ifsc_code', models.CharField(blank=True, max_length=50, null=True)),
                ('swift_code', models.CharField(blank=True, max_length=50, null=True)),
                ('iban_code', models.CharField(blank=True, max_length=50, null=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='config.Company')),
            ],
            options={
                'verbose_name': 'BankDetail',
                'verbose_name_plural': 'BankDetails',
            },
        ),
    ]
