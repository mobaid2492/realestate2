from django.contrib import admin
from .models import BankDetail,Company,Office,Spotlight,Gallery,MediaMentions,Document

# Register your models here.
admin.site.register(BankDetail)
admin.site.register(Company)
admin.site.register(Office)
admin.site.register(Spotlight)
admin.site.register(Gallery)
admin.site.register(MediaMentions)
admin.site.register(Document)

