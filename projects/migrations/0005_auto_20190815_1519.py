# Generated by Django 2.2.2 on 2019-08-15 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_transaction'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='propery_ref',
            new_name='property_ref',
        ),
        migrations.AddField(
            model_name='property',
            name='emi_plan',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'Full Payment'), (1, '1 Year Plan'), (2, '2 Year Plan'), (3, '3 Year Plan')], null=True),
        ),
    ]
