# Generated by Django 2.2.2 on 2019-08-06 20:02

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PriceRule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('premium_price', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('project_name', models.CharField(max_length=100)),
                ('pro_category', models.CharField(choices=[('R', 'Residential'), ('C', 'Commercial')], max_length=1)),
                ('project_address', models.TextField(blank=True, null=True)),
                ('pin_code', models.IntegerField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('city', models.CharField(blank=True, max_length=100, null=True)),
                ('locality', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(blank=True, null=True)),
                ('geo_code_lat', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('geo_code_long', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('contact_person_name', models.CharField(blank=True, max_length=100, null=True)),
                ('contact_person_phone', models.BigIntegerField(blank=True, null=True)),
                ('contact_person_mail', models.EmailField(blank=True, max_length=254, null=True)),
                ('cover_image', models.ImageField(blank=True, null=True, upload_to='images/project/')),
                ('layout', models.FileField(blank=True, null=True, upload_to='files/project/')),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('deactivatedAt', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('serial_no', models.IntegerField()),
                ('status_code', models.CharField(choices=[('V', 'Vacant'), ('H', 'Hold'), ('B', 'Booked'), ('A', 'Alloted'), ('R', 'Registered')], max_length=1)),
                ('cust_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='customers.Customer')),
                ('price_rule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.PriceRule')),
            ],
            options={
                'verbose_name': 'Property',
                'verbose_name_plural': 'Properties',
            },
        ),
        migrations.CreateModel(
            name='TempBooking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('booking_date', models.DateTimeField()),
                ('booking_revoke_date', models.DateTimeField()),
                ('status', models.BooleanField(default=True)),
                ('customer_ref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customers.Customer')),
                ('property_ref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Property')),
            ],
            options={
                'verbose_name': 'tempBooking',
                'verbose_name_plural': 'tempBookings',
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('section_prefix', models.CharField(max_length=4)),
                ('dimensions', models.CharField(max_length=50)),
                ('area', models.IntegerField()),
                ('cost_per_unit', models.IntegerField()),
                ('project_ref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Project')),
            ],
            options={
                'verbose_name': 'Section',
                'verbose_name_plural': 'Sections',
            },
        ),
        migrations.AddField(
            model_name='property',
            name='section_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Section'),
        ),
        migrations.CreateModel(
            name='EMI',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pay_day', models.DateField(blank=True)),
                ('opening_balance', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('emi', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('intrest_paid', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('closing_balance', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('status', models.BooleanField(default=False)),
                ('property_ref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Property')),
            ],
            options={
                'verbose_name': 'emi',
                'verbose_name_plural': 'emis',
            },
        ),
    ]
