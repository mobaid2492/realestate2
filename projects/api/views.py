from rest_framework import viewsets,status,generics
from rest_framework.views import APIView
from rest_framework.response import Response
from customers.api.serializers import CustomerSerializer
from customers.models import Customer
from projects.models import Property,PriceRule
from projects.api.serializers import PropertySerializer 



class PropertyAPIView(APIView):
    def get(self, request,section_id):
        queryset= Property.objects.all().filter(section_id=section_id)
        serializer_context = {
            'request': request,
        }
     
        serializer= PropertySerializer(instance=queryset,context=serializer_context, many=True)
        return Response(serializer.data, headers={'Access-Control-Allow-Origin': '*'})

class CustomerRudView(generics.RetrieveUpdateDestroyAPIView):
  lookup_field=  'id'
  serializer_class= CustomerSerializer
  def get_queryset(self):    
    queryset = Customer.objects.all() # TODO
    return queryset
  #def get_object(self):
  #,'section_id','price_rule'
    #contact_no=self.kwargs.get('contact_no')
    #return Customer.objects.get(contact_no=contact_no)

class CustomerCreateApiView(generics.CreateAPIView):
    lookup_field= 'pk'
    serializer_class= CustomerSerializer    
    def get_queryset(self):        
        queryset= Customer.objects.all()
        return queryset