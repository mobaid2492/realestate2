from django.urls import path,include
from . import views


app_name="project"
urlpatterns = [  
    
    path('<int:id>', views.project_profile, name='project_profile'),
    path('availability/<int:id>', views.availability, name='availability'),
    path('booking/project/<int:id>/',views.booking, name='booking'),
    path('', views.project, name='home'),  
    #path('test', views.test),
    
    
]
