import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'realEstate2.settings')

app = Celery('realEstate2')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()