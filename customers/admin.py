from django.contrib import admin
from .models import Customer
from projects.models import EMI,Transaction
# Register your models here.
admin.site.register(Customer)
admin.site.register(EMI)
admin.site.register(Transaction)