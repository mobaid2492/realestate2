from django.db import models
from associate.models import Associate
from django.utils import timezone

#from projects.models import Property

# Create your models here.

class Customer(models.Model):
    associate_ref= models.ForeignKey(Associate, on_delete=None,blank=True,null=True)
    name= models.CharField(max_length=100)
    country_code=models.CharField(max_length=50,blank=True,null=True)   
    contact_no=models.BigIntegerField(unique=True)
    alternate_no=models.BigIntegerField(blank=True,null=True)
    email= models.EmailField(blank=True,null=True)
    pan_no=models.CharField(max_length=10,blank=True, null=True)
    address=models.CharField(max_length=100)
    createdAt= models.DateTimeField(auto_now_add=True)
    #date_created     

    class Meta:
        verbose_name = ("Customer")
        verbose_name_plural = ("Customers")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Customer_detail", kwargs={"pk": self.pk})


