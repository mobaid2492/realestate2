# Generated by Django 2.2.2 on 2019-08-06 20:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('associate', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('country_code', models.CharField(blank=True, max_length=50, null=True)),
                ('contact_no', models.BigIntegerField(unique=True)),
                ('alternate_no', models.BigIntegerField()),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('address', models.CharField(max_length=100)),
                ('createdAt', models.DateTimeField(auto_now_add=True)),
                ('associate_ref', models.ForeignKey(blank=True, null=True, on_delete=None, to='associate.Associate')),
            ],
            options={
                'verbose_name': 'Customer',
                'verbose_name_plural': 'Customers',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pay_method', models.CharField(choices=[('C', 'Cash Payment'), ('Q', 'Cheque'), ('N', 'NEFT'), ('D', 'Bank Draft')], max_length=1)),
                ('date', models.DateField(auto_now_add=True)),
                ('reference_no', models.CharField(blank=True, max_length=50, null=True)),
                ('amount', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('customer_ref', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='customers.Customer')),
            ],
            options={
                'verbose_name': 'Transaction',
                'verbose_name_plural': 'Transactions',
            },
        ),
    ]
