from django import forms
from .models import Customer
class CustomerForm(forms.ModelForm):
    class Meta:
        model= Customer
        fields= [
        'name',
        'associate_ref',
        'contact_no',
        'pan_no',
        'country_code',
        'alternate_no',
        'email',
        'address'
        ]
class CustomerCreateForm(forms.Form):
    name=forms.CharField(max_length=100, required=True)
    associate_ref=forms.CharField(max_length=10, required=True)
    contact_no=forms.IntegerField(required=False)
    country_code=forms.CharField( max_length=5,required=False)
    pan_no=forms.CharField(max_length=10)
    alternate_no=forms.IntegerField(required=False)
    email= forms.EmailField(required=False)
    address=forms.CharField( max_length=400, required=False)
