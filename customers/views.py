from django.shortcuts import render,redirect
from customers.forms import CustomerCreateForm,CustomerForm
from .models import Customer
from config.models import Company
from associate.models import Associate
from django.urls import reverse
from projects.models import TempBooking, EMI,Transaction
from django.contrib import auth, messages
# Create your views here.
def getCompany():
  company_details=Company.objects.all()
  return company_details

def login(request):
  context={
           "details":getCompany(),
                                
  }
  return render(request ,'customers/login.html', context)
def loader(request):
  context={
           "details":getCompany(),
                                
  }
  return render(request ,'customers/loader.html', context)
def home(request,id):
  customer=Customer.objects.get(id=id)

  emi=EMI.objects.all().filter(property_ref__customer_ref=customer)
  tx=Transaction.objects.all().filter(property_ref__customer_ref=id)
  temp_bookings= TempBooking.objects.all().filter(customer_ref=id).order_by('id')
  #bookings= Booking.objects.all().filter(customer_ref=id)
  form=CustomerForm(request.POST or None,instance=customer)
  if form.is_valid():
    form.save()
    form=CustomerForm(request.POST or None)
   
  context={
           "details":getCompany(),
           "form":form,
           "customer":customer,
           "emi":emi,
           "tx":tx, 
          # "bookings":bookings,
           "temp_bookings":temp_bookings,        
               
  }
  return render(request ,'customers/home.html',context) 
  
def register(request):  
  form=CustomerCreateForm(request.POST or None)
  if form.is_valid():
        associate_ref = form.cleaned_data['associate_ref']
        associate = Associate.objects.get(user__username=associate_ref)
        name = form.cleaned_data['name']
        contact_no = form.cleaned_data['contact_no']
        country_code = form.cleaned_data['country_code']
        alternate_no = form.cleaned_data['alternate_no']
        email = form.cleaned_data['email']
        address = form.cleaned_data['address']
        
        try:
            new_customer = Customer(name=name, contact_no=contact_no, associate_ref=associate,
                                country_code=country_code, alternate_no=alternate_no,
                                email=email,address=address)
            new_customer.save()
            return redirect(reverse('customers:loader'))
        except :
            messages.error(request, 'Customer with this phone already exists')
        
        messages.success(request, 'Customer Registered successfully')
    
  elif request.POST:
    form=CustomerCreateForm(request.POST or None)
    messages.success(request,'Invalid details')
  context={
           "details":getCompany(),
           "form":form          
               
  }
  return render(request ,'customers/registration.html',context)
  
  