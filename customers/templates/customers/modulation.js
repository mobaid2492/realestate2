function timeModulation(time, date) {
    time.trim();
    hour = parseInt(time.charAt(0)) + 5;
    minutes = parseInt(time.slice(time.indexOf(":") + 1, time.indexOf(":") + 3)) + 30;
    flag = time.slice(time.indexOf(":") + 4, time.indexOf(":") + 8);
    var dd = "";
    if (minutes > 60) {
      minutes = minutes - 60;
      hour = hour + 1;
    }
    if (hour > 12) {
      hour = hour - 12;
      if (flag == "a.m.") {
        flag = "p.m.";
      }
      else {
        flag = "a.m.";
        dd = dateChange(date, 1)
      }
    }
    else if (hour == 12) {
      if (flag == "a.m.") {
        flag = "p.m.";
      }
      else {
        flag = "a.m.";
        dd = dateChange(date, 1)
      }
    }
    else {
      dd = dateChange(date, 0)
    }
    var str = "";
    if (minutes < 10) {
      str = " " + hour + ":0" + minutes + " " + flag;
    }
    else {
      str = " " + hour + ":" + minutes + " " + flag;
    }
    str = dd + str;
    //  console.log(str)
    return (str);
  }
  function dateChange(date, val) {
    date.trim();
    // hour = parseInt(time.charAt(0)) + 5;
    dd = parseInt(date.slice(date.indexOf(",") - 2, date.indexOf(","))) + val;
    yy = date.slice(date.indexOf(",") + 1, date.length)

    str = dd + "," + yy + " at ";
    // console.log(str)
    return str;
  }
