from django import forms

class PointofSales(forms.Form):
    amount=forms.DecimalField()
    property_ref=forms.IntegerField()
    associate_ref=forms.CharField(max_length=10, required=True)
    customer_no=forms.IntegerField()
class LoginAmountDeposit(forms.Form):
    METHOD = (             
        ('C', 'Cash Payment'),
        ('Q', 'Cheque'),
        ('N', 'NEFT'),
        ('D', 'Bank Draft'),        
    )
    payment_mode=forms.ChoiceField(choices=METHOD, required=True)
    reference_no=forms.CharField(max_length=50, required=True)
    property_ref=forms.IntegerField(required=True)
    amount=forms.DecimalField(required=True)
    description=forms.CharField(max_length=100,required=True)