from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib import auth, messages
from django.utils.timezone import timezone
import datetime
from .forms import PointofSales,LoginAmountDeposit
from projects.models import Project, Section, Property, PriceRule, EMI,Transaction
from projects.forms import ProjectForm, SectionForm, PropertyForm
from customers.models import Customer
from customers.forms import CustomerCreateForm,CustomerForm
from reports.models import *
from reports.forms import SaleForm,BookingForm,AssociateTransactionForm,RedeemForm,OtherExpencesForm,PayoutRuleForm,RewardsRuleForm,PriceRuleForm
from associate.models import Associate
from associate.forms import AssociateForm,AssociateEditForm
from reports.tasks import *
from reports.calculations import *
from user.models import User
from datetime import timedelta
from django.utils.timezone import now
from webapp.models import Carrer,Contact

# side menu options
url = "/owner/"
menu = [
    {"id": "1",
     "name": "Dashboard",
     "url": url+"",
     "icon": "fa-tachometer-alt",
     "active": "",
     "sub": [

     ],
     },
    {"id": "2",
     "name": "Projects",
     "url": url+"project",
     "icon": "fa-landmark",
     "active": "",
     "sub": [
         {"name": "View Projects",
          "url": url+"project", },
         {"name": "PLC Rules",
             "url": url+"project/plc", },

     ],
     },
    {"id": "3",
     "name": "Customers",
     "url": url+"customers",
     "icon": "fa-user",
     "active": "",
     "sub": [
         {"name": "View all Customers",
          "url": url+"customers"
          },
         {"name": "Add new Customers",
          "url": url+"customers/add"
          },


     ], },
    {"id": "4",
     "name": "Assocates",
     "url": url+"associates",
     "icon": "fa-users",
     "active": "",
     "sub": [{
         "name": "All Associates",
         "url": url+"associates",
     },
         {
         "name": "Add new Associate",
         "url": url+"associates/add",
     },


     ], },
    {"id": "5",
     "name": "Buisness",
     "url": url+"sales/pos/3",
     "icon": "fa-briefcase",
     "active": "",
     "sub": [
         {"name": "New Booking",
          "url": url+"sales/pos/", },
         {"name": "Final Bookings",
             "url": url+"sales/salesbook", },
         {"name": "Properties By Status",
             "url": url+"sales/status/", },

     ], },
    {"id": "6",
     "name": "Finance       ",
     "url": url+"finance/login-amount",
     "icon": "fa-rupee-sign",
     "active": "",
     "sub": [
           {"name": "Login Amount",
            "url": url+"finance/login-amount", },
           {"name": "Payouts",
            "url": url+"finance/payouts", },
         {"name": "Rewards",
             "url": url+"finance/rewards", },
         {"name": "Other Expences",
             "url": url+"finance/other-expences/1", },

     ], },
    {"id": "7",
     "name": "Reports",
     "url": url+"sales/pos/3",
     "icon": "fa-file-invoice-dollar",
     "active": "",
     "sub": [
         {"name": "Sales Report",
           "url": url+"reports/sales-report", },
         {"name": "Buisness Report",
          "url": url+"reports/buisness-report", },
         {"name": "Emi Report",
          "url": url+"reports/emi-report", },

     ], },
    {"id": "8",
     "name": "User Management",
     "url": url+"sales/pos/3",
     "icon": "fa-user-lock",
     "active": "",
     "sub": [ 
         {"name": "View All Users",
          "url": "/user/user-list", },
        {"name": "Add New Users",
          "url": "/user/add-user", },


     ], },
    {"id": "9",
     "name": "Company Policy",
     "url": url+"",
     "icon": "fa-file-signature",
     "active": "",
     "sub": [
         {"name": "Payout Rules",
          "url": url+"policy/payout/", },
         {"name": "Reward Rules",
          "url": url+"policy/rewards/", },
         

     ], }
]
def general_access(request,allowedUser):
    user=request.user
    ret=redirect('owner:home')
    if request.user.is_anonymous:
        ret=redirect('user:login')
    else:    
        for user_type in allowedUser:
            if user_type == user.user_type:
                ret=None     
        if user.user_type == 7:
            ret=redirect('associate:home')
        if ret is not None:
            messages.error(request,'Access Denied!!')            
    return ret

def sidebarActive(id):
    for menuitem in menu:
        if menuitem['id'] == id:
            menuitem['active'] = "active"
        else:
            menuitem['active'] = ""


def index(request):
    sidebarActive("1")
    
    carrer=Carrer.objects.all()
    contactForm=Contact.objects.all()
    temp_bookings=TempBooking.objects.all()
    context = {
        "menu": menu,
        "user": request.user,
        "career":carrer,
        "contactForm":contactForm,
        "temp_bookings":temp_bookings
    }
    return render(request, 'owner/index.html', context)
    # 39xxrqd2yi


##############################  PROJECTS  ######################################################

def plcRules(request):
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    rules = PriceRule.objects.all()
    context = {
        "menu": menu,
        "rules": rules,
    }
    return render(request, 'projects/admin_plc_rules.html', context)


def projects(request):
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    project_list = Project.objects.all()
    form = ProjectForm(request.POST or None)
    user = request.user
    if form.is_valid():
        if user.user_type == 1 or user.user_type == 2:
            form.save()
            form = ProjectForm(request.POST or None)
            messages.success(request, 'Project has been added successfully')
            return redirect('owner:projects')
        else:
            messages.error(request, 'Access Denied: You are not authorised!')
            return redirect('owner:projects')
    context = {
        "projects": project_list,
        "form": form,
        "menu": menu,
        "user": request.user
    }
    return render(request, 'projects/admin_projects.html', context)


def projectView(request, id):
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    try:
        project = Project.objects.get(id=id)
    except :
        messages.error(request, 'The Project does not exists')        
        return redirect('owner:projects')
    sections = Section.objects.all().filter(project_ref=id)
    properties = Property.objects.select_related('section_ref','price_rule','customer_ref').all()
    form = SectionForm(request.POST or None)
    user = request.user
    if form.is_valid():
        if user.user_type == 1 or user.user_type == 2:
            form.save()
            form = SectionForm(request.POST or None)
            messages.success(request, 'Section has been added successfully')
            return redirect('owner:project_view', id=id)
        else:
            messages.error(request, 'Access Denied: You are not authorised!')
            return redirect('owner:projects')
    context = {
        "project": project,
        "menu": menu,
        "form": form,
        "sections": sections,
        "properties": properties,
        "user": user
    }
    return render(request, 'projects/admin_projectView.html', context)


def projectEdit(request, id):
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    try:
        project = Project.objects.get(id=id)
    except :
        messages.error(request, 'The Project does not exists')        
        return redirect('owner:projects')
    sections = Section.objects.all().filter(project_ref=id)
    form = ProjectForm(request.POST or None, instance=project)
    user = request.user
    if user.user_type == 1 or user.user_type == 2:
        if form.is_valid():
            form.save()
            messages.success(request, 'Project has been edited successfully')
            return redirect('owner:project_view', id=id)

    else:
        messages.error(request, 'Access Denied: You are not authorised!')
        return redirect('owner:projects')

    context = {
        "project": project,
        "menu": menu,
        "form": form,
        "sections": sections,

    }
    return render(request, 'projects/admin_projectEdit.html', context)


def projectDelete(request, id):
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    try:
        project = Project.objects.get(id=id)
    except :
        messages.error(request, 'The Project does not exists')        
        return redirect('owner:projects')

    user=request.user
    if user.user_type == 1:
        project.delete()
        messages.success(request, 'Project has been deleted Successfully')
        return redirect('owner:projects')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:projects')
##################################### SECTIONS #####################################################


def sectionView(request, proid, id):
    sidebarActive("2")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    try:
        sections = Section.objects.get(id=id)
    except :
        messages.error(request, 'The Section does not exists')        
        return redirect('owner:projects')
    try:
        project = Project.objects.get(id=proid)
    except :
        messages.error(request, 'The Project does not exists')        
        return redirect('owner:projects')
    properties = Property.objects.select_related('price_rule','customer_ref').all().filter(section_ref=id)
    price_rules = PriceRule.objects.all()
    form = PropertyForm(request.POST or None)
    user = request.user
    if form.is_valid():
        if user.user_type == 1 or user.user_type == 2:
            form.save()
            form = PropertyForm(request.POST or None)
            messages.success(request, 'Section has been added Successfully!')
        else:
            messages.error(request, 'Access Denied')
            return redirect('owner:projects')
    context = {
        "project": project,
        "menu": menu,
        "form": form,
        "section": sections,
        "properties": properties,
        "price_rules": price_rules,
    }
    return render(request, 'projects/admin_sectionView.html', context)


def sectionEdit(request, id, proid):
    sidebarActive("2")
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    try:
        section = Section.objects.get(id=id)
    except :
        messages.error(request, 'The Section does not exists')        
        return redirect('owner:projects')
    user = request.user
    form = SectionForm(request.POST or None, instance=section)
    if user.user_type == 1 or user.user_type == 2:
        if form.is_valid():
            form.save()
            messages.success(request, 'Section has been edited successfully!')
            return redirect('owner:section_view', proid=section.project_ref.id, id=section.id)
    else:
        messages.error(request, 'Access Denied: You are not authorised')
        return redirect('owner:section_view', proid=section.project_ref.id, id=section.id)

    context = {
        "section": section,
        "menu": menu,
        "form": form
    }
    return render(request, 'projects/admin_sectionEdit.html', context)


def sectionDelete(request, id, proid):
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    try:
        section = Section.objects.get(id=id)
    except :
        messages.error(request, 'The Section does not exists')        
        return redirect('owner:projects')
    user = request.user
    if user.user_type == 1:
        section.delete()
        messages.success(request, 'Section has been edited Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised')
    return redirect('owner:project_view', id=section.project_ref.id)

############################## PROPERTY #######################################################


def propertyView(request, id):
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    try:
        properties = Property.objects.get(id=id)
    except:
        messages.error(request, 'The property does not exists')        
        return redirect('owner:projects')
    context = {
        "property": properties,
        "menu": menu,
    }
    return render(request, 'projects/admin_propertyView.html', context)


def propertyEdit(request, id):
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    sidebarActive("2")
    user = request.user
    try:
        prop = Property.objects.get(id=id)
    except:
        messages.error(request, 'The property does not exists')        
        return redirect('owner:projects')
    # proid=Property.section_ref.project_ref.id
    # prop.section_ref=Section.objects.all().filter(project_ref=proid)
    price_rules = PriceRule.objects.all()
    form = PropertyForm(request.POST or None, instance=prop)
    if user.user_type == 1 or user.user_type == 2:
        if form.is_valid():
            form.save()
            messages.success(request, 'Property has been edited Successfully')
            return redirect('owner:property_view', id=prop.id)
    else:
        messages.error(
            request, 'Access Denied: You are not authorised to perform this action')
        return redirect('owner:property_view', id=prop.id)

    context = {
        "property": prop,
        "menu": menu,
        "form": form,
        "price_rules": price_rules,
    }
    return render(request, 'projects/admin_propertyEdit.html', context)


def propertyDelete(request, id):
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    try:
        properties = Property.objects.get(id=id)
        properties.delete()
        messages.success(request, 'The property Deleted Successfully!!')        
    except:
        messages.error(request, 'The property does not exists')        
        return redirect('owner:projects')
    return redirect('owner:projects')

#######################################################################################################################
#Customers                                           Customers                                              Customers
#######################################################################################################################


def customerListView(request):
    sidebarActive("3")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    customers = Customer.objects.all()
    properties = Property.objects.all()
    context = {
        "customer_list": customers,
        "menu": menu,

        "properties": properties

    }
    return render(request, 'customers/admin_customerList.html', context)


def customerView(request, id):
    sidebarActive("3")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    customer = Customer.objects.get(id=id)
    properties = Property.objects.all().filter(customer_ref=id)
    emi = EMI.objects.all().filter(property_ref__customer_ref=id)
    tx = Transaction.objects.all().filter(property_ref__customer_ref=id)
    paid_emi = EMI.objects.all().filter(property_ref__customer_ref=id).filter(
        status=True).aggregate(Count('emi'))
    pending_emi = EMI.objects.all().filter(property_ref__customer_ref=id).filter(
        status=False).aggregate(Count('emi'))
    context = {
        "customer": customer,
        "menu": menu,
        "properties": properties,
        "amount": "amount",
        "emi": emi,
        "pending_emi": pending_emi,
        "paid_emi": paid_emi,
        "tx": tx,
    }
    return render(request, 'customers/admin_customerView.html', context)


def createCutomer(request):    
    sidebarActive("3")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    form = CustomerCreateForm(request.POST or None)
    if form.is_valid():
        associate_ref = form.cleaned_data['associate_ref']
        associate = Associate.objects.get(user__username=associate_ref)
        name = form.cleaned_data['name']
        contact_no = form.cleaned_data['contact_no']
        pan_no = form.cleaned_data['pan_no']
        country_code = form.cleaned_data['country_code']
        alternate_no = form.cleaned_data['alternate_no']
        email = form.cleaned_data['email']
        address = form.cleaned_data['address']
        new_customer = Customer(name=name, contact_no=contact_no, associate_ref=associate,
                                country_code=country_code,pan_no=pan_no, alternate_no=alternate_no,
                                email=email,address=address)
        try:
            new_customer.save()
        except :
            messages.error(request, 'Customer with this phone/email already exists')
        
        messages.success(request, 'Customer saved successfully')
        return redirect('owner:pos_list')
        # form.save()
    elif request.POST:
        messages.error(request, 'Incorrect form details')

    context = {
        "menu": menu,
        "form": form

    }
    return render(request, 'customers/admin_customerCreate.html', context)
def customerEdit(request,id):
    sidebarActive("3")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    customer= Customer.objects.get(id=id)
    associate_list=Associate.objects.select_related('user').only('id','user').all().exclude(user__username=None)
    if request.POST:
        form=CustomerForm(request.POST, instance=customer)
        if form.is_valid():
            new_customer=form.save(commit=False)            
            new_customer.save()
            return redirect('owner:customer_view',id=customer.id )
        else:
            messages.error(request, 'Incorrect form details')
    else:
        form=CustomerForm(instance=customer)

    context = {
        "customer":customer,
        "menu": menu,
        "form":form,
        "heading": "Customers Edit",
        "associate_list":associate_list,
    }
    return render(request, 'customers/admin_customerEdit.html', context)
#######################################################################################################################
#Associate                      Associate
#######################################################################################################################


def associates(request):
    sidebarActive("4")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    associate_list = Associate.objects.all()
    customers = Customer.objects.all()
    context = {

        "menu": menu,
        "associate_list": associate_list,
        "customers": customers,

    }
    return render(request, 'associate/associateList.html', context)


def associateView(request, id):
    
    sidebarActive("4")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    #sales=Sale.objects.select_related('property_ref').filter(associate_ref_id=id)
    downline_count = Associate.objects.all().filter(parent=id).aggregate(Count('id'))
    downlines = Associate.objects.all().filter(parent=id)
    customers_list = Customer.objects.all().filter(associate_ref=id)
    properties = Property.objects.all()
    #ssociateTask.createTree.delay(id)
    #AssociateTask.get_downlineTree.delay(id)
    sales=AssociateTask.getDownlineSalesTree(id)
    context = {

        "menu": menu,
        "associate": associate,
        "downline_count": downline_count,
        "customers_list": customers_list,
        "properties": properties,
        "sales":sales,
        "downlines":downlines,
       

    }
    return render(request, 'associate/associateView.html', context)


def associateCreate(request):
    sidebarActive("4")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    form = AssociateForm(request.POST, request.FILES or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        photo = form.cleaned_data['photo']
        password = form.cleaned_data['password']
        upline=None
        contact_no = form.cleaned_data['contact_no']
        alternate_no = form.cleaned_data['alternate_no']
        address = form.cleaned_data['address']
        pincode = form.cleaned_data['pincode']
        gender = form.cleaned_data['gender']
        date_of_birth = form.cleaned_data['date_of_birth']
        pan_no = form.cleaned_data['pan_no']
        bank_name = form.cleaned_data['bank_name']
        branch_name = form.cleaned_data['branch_name']
        account_no = form.cleaned_data['account_no']
        ifsc_code = form.cleaned_data['ifsc_code']
        name = first_name+" "+last_name
        count=0
        try:
            uplineID = form.cleaned_data['uplineID']
            upline = Associate.objects.get(user__username=uplineID)
        except upline is None:
            count=1
            messages.error(request, 'Wrong upline ID')
        try:
            associate = Associate.objects.get(
                contact_no=form.cleaned_data['contact_no'])
            associate = Associate.objects.get(email=form.cleaned_data['email'])
            associate = Associate.objects.get(email=form.cleaned_data['pan_no'])
            messages.error(
                request, 'The requested Associate is already registered')
        except Associate.DoesNotExist:
            if count==0:                
                newuser = AssociateTask.create_user_accounts(
                    email, first_name, last_name, photo, 7, password)
                messages.success(request, 'The Associate has been Saved')
                Associate.objects.create(user=newuser, name=name, parent=upline, country_code='+91',
                                        contact_no=contact_no, alternate_no=alternate_no, email=email,
                                        address=address, pincode=pincode, photo=photo, gender=gender,
                                        date_of_birth=date_of_birth, pan_no=pan_no, bank_name=bank_name,
                                        branch_name=branch_name, account_no=account_no, ifsc_code=ifsc_code)
                return redirect('owner:associates')
    elif request.POST:
        messages.error(request, 'Incorrect Form Details')

    context = {
        "menu": menu,
        "form": form,

    }
    return render(request, 'associate/associateCreate.html', context)


def associateEdit(request, id):
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    sidebarActive("4")
    associate_ref = Associate.objects.get(id=id)
    form = AssociateEditForm(request.POST, request.FILES or None)
    if form.is_valid():
        messages.error(request, 'Wrong upline ID')
        email = form.cleaned_data['email']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        photo = form.cleaned_data['photo']
        #password = form.cleaned_data['password']
        uplineID = form.cleaned_data['uplineID']
        contact_no = form.cleaned_data['contact_no']
        alternate_no = form.cleaned_data['alternate_no']
        address = form.cleaned_data['address']
        pincode = form.cleaned_data['pincode']
        gender = form.cleaned_data['gender']
        date_of_birth = form.cleaned_data['date_of_birth']
        pan_no = form.cleaned_data['pan_no']
        bank_name = form.cleaned_data['bank_name']
        account_no = form.cleaned_data['account_no']
        ifsc_code = form.cleaned_data['ifsc_code']
        name = first_name+" "+last_name
        count=0
        try:
            upline = Associate.objects.get(user__username=uplineID)
        except Associate.DoesNotExist:
            count=1
            messages.error(request, 'Wrong upline ID'+uplineID)
        associate_user = User.objects.get(id=associate_ref.user_id)
        associate_user.photo = photo
        associate_user.first_name = first_name
        associate_user.last_name = last_name
        if count==0:
            associate_user.save()
        associate_ref.name = name
        associate_ref.parent = upline
        associate_ref.contact_no = contact_no
        associate_ref.alternate_no = alternate_no
        associate_ref.pincode = pincode
        associate_ref.photo = photo
        associate_ref.gender = gender
        associate_ref.date_of_birth = date_of_birth
        associate_ref.pan_no = pan_no
        associate_ref.bank_name = bank_name
        associate_ref.account_no = account_no
        associate_ref.ifsc_code = ifsc_code
        if count==0:
            associate_ref.save()
            return redirect('owner:associates')
    elif request.POST:
        messages.error(request, 'Incorrect Form Details')

    context = {
        "menu": menu,
        "form": form,
        "associate": associate_ref

    }
    return render(request, 'associate/associateEdit.html', context)
def associateEditPhoto(request, id):
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    sidebarActive("4")
    associate_ref = Associate.objects.get(id=id)    
    if request.POST:        
        photo = request.FILES['photo']   
        associate_ref.photo = photo   
        associate_ref.save()
        return redirect('owner:associates')


   

#hold payments and terminate
def associateSuspend(request, id):
    user = request.user
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    upline_user = associate.user
    if user.user_type == 1:
        upline_user.is_active = False
        upline_user.save()
        associate.status = False
        associate.save()
        messages.success(request, 'Associate has been Suspended Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:associates')


def associateActivate(request, id):
    user = request.user
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    upline_user = associate.user
    if user.user_type == 1:
        upline_user.is_active = True
        upline_user.save()
        associate.status = True
        associate.save()
        messages.success(request, 'Associate has been Activated Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:associates')

def associateTerminate(request,id):
    user = request.user
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    upline_user = associate.user
    if user.user_type == 1:
        upline_user.is_active = False
        upline_user.save()
        associate.status = False
        associate.save()
        #AssociateTask.terminateAssociate()
        messages.success(request, 'Associate has been Terminated Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:associates')
def associateHoldPayments(request,id):
    user = request.user
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    if user.user_type == 1:        
        associate.is_payment_on_hold=True        
        associate.save()
        messages.success(request, 'Associate Payments has been put on hold')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:associate_view', id=associate.id)
def associateReleasePayments(request,id):
    user = request.user
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    associate = Associate.objects.get(id=id)
    if user.user_type == 1:        
        associate.is_payment_on_hold=False  
        associate.save()         
        messages.success(request, 'Associate Payments has been Released Successfully')
    else:
        messages.error(request, 'Access Denied: You are not authorised!')
    return redirect('owner:associate_view' , id=associate.id)

#######################################################################################################################
#Sales                                           Sales                                               Sales
#######################################################################################################################
def propertyByStatus(request):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    vacant=Property.objects.filter(status_code='V').aggregate(Count('id'))
    hold=Property.objects.filter(status_code='H').aggregate(Count('id'))
    booked=Property.objects.filter(status_code='B').aggregate(Count('id'))
    alloted=Property.objects.filter(status_code='A').aggregate(Count('id'))
    registered=Property.objects.filter(status_code='R').aggregate(Count('id'))
    if ret_var is not None:
        return ret_var
    context = {
       
        "menu": menu,
        "vacant":vacant,
        "hold":hold,
        "booked":booked,
        "alloted":alloted,
        "registered":registered
    }
    return render(request, 'owner/sales/property_status.html', context)
def propertyListByStatus(request,slug):
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    form=LoginAmountDeposit(request.POST or None)
    if form.is_valid():
        payment_mode=form.cleaned_data['payment_mode']
        reference_no=form.cleaned_data['reference_no']
        property_id=form.cleaned_data['property_ref']
        amount=form.cleaned_data['amount']
        desc=form.cleaned_data['description']
        property_ref=Property.objects.get(id=int(property_id))
        messages.success(request,'Amount has been deposited')
        FinanceTask.depositLoginAmount(payment_mode,reference_no,property_ref,Decimal(amount),desc)
        #return redirect('owner:login_amount')
    else:
        if request.POST:
            messages.error(request,'Incorrect form details')
    property_list=Property.objects.filter(status_code=slug).exclude(status_code='V')
    transactions=Transaction.objects.only('sale_value','balance','property_ref').exclude(property_ref__status_code='V').order_by('date')
    context = {
        "menu": menu,
        "property_list":property_list,
        "form":form,
        "transactions":transactions,
        "slug":"slug",
        "heading":"Properties By Status",
    }
    return render(request, 'owner/finance/login_amount_deposit.html', context)

def sales(request):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    prop_list = Property.objects.select_related(
        'price_rule', 'section_ref').all().order_by('serial_no')

    context = {
        "prop_list": prop_list,
        "menu": menu,

    }
    return render(request, 'owner/sales/pos_list.html', context)


def salesBook(request):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    sales = Sale.objects.all().order_by('createdAt')
    context = {
        "sales": sales,
        "menu": menu,

    }
    return render(request, 'owner/sales/booking_list.html', context)


def pointofSales(request, id):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    prop = Property.objects.get(id=id)
    associate_list = Associate.objects.only('name', 'id').all()
    monthly_sales = Sale.objects.filter(
        associate_ref=9, createdAt__month=8).aggregate(Sum('amount'))
    form = PointofSales(request.POST or None)
    ret = False
    if form.is_valid():
        #associate_id = form.cleaned_data['associate_ref']
        #associate = Associate.objects.get(user__username=associate_id)
        customer = Customer.objects.get(
            contact_no=form.cleaned_data['customer_no'])
        associate=customer.associate_ref
        new_sale = Sale(
            amount=form.cleaned_data['amount'], property_ref=prop, status=True, associate_ref=associate)
        prop.customer_ref = customer
        prop.save()
        new_sale.save()
        ret = PayoutTask.update_lifetime_payout.delay(associate.id)
        return redirect('owner:booking_deposit', id=prop.id)
    elif request.POST :
        messages.error(request, 'Incorrect form details')
    context = {
        "property": prop,
        "menu": menu,
        "form": form,
        "cal_payout": "associate"

    }
    return render(request, 'owner/sales/pointofSales.html', context)

def bookingReceipt(request,id):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    sales = Sale.objects.get(id=id)
    context = {
        "sales": sales,
        "menu": menu,
    }
    return render(request, 'owner/sales/booking_recipt.html', context)
#######################################################################################################################
#Finance                                           Finance                                               Finance
#######################################################################################################################
def depositBooking(request,id):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    prop = Property.objects.get(id=id)
    form=BookingForm(request.POST or None)
    
    if request.POST:
        total_amount= request.POST['total_amount']
        booking_amount= request.POST['booking_amount']
        payment_mode= request.POST['payment_mode']
        emi_plan= request.POST['emi_plan']
        reference_no= request.POST['reference_no']
        BookingTask.completeBooking(payment_mode,prop,reference_no,Decimal(booking_amount),Decimal(total_amount),int(emi_plan),prop.customer_ref.associate_ref)
        messages.success(request, 'Booking Completed for '+str(prop.section_ref.section_prefix)+'-'+str(prop.serial_no) )
        return redirect('owner:sales_book')
       
    elif request.POST :
        messages.error(request, 'Incorrect form details')
    context = {
        "menu": menu,
        "property":prop,
       
        
    }
    return render(request, 'owner/finance/booking_deposit.html', context)

def paymentReceipt(request,id):
    sidebarActive("5")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    transaction = Transaction.objects.get(id=id)
    context = {
        "entry": transaction,
        "menu": menu,
    }
    return render(request, 'owner/finance/payment_receipt.html', context)

def loginAmount(request):
    sidebarActive("6")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    if request.POST:
        phone=request.POST['phone']
        page=request.POST['page']
        try:
            customer=Customer.objects.get(contact_no=int(phone))
            if page == 'login_amt':
                return redirect('owner:deposit_login_amount',id=customer.id)
            else:
                return redirect('owner:deposit_emi',id=customer.id)
            messages.success(request,'Customer Found')
        except:
            messages.error(request,'Customer not Found')     
    transactions=Transaction.objects.select_related('property_ref').all()
    context = {
        "menu": menu,
        "transactions":transactions
    }
    return render(request, 'owner/finance/login_amount.html', context)
def depositEmi(request,id):
    sidebarActive("6")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    customer=Customer.objects.get(id=id)
    emi=EMI.objects.filter(property_ref__customer_ref=customer)
    if request.POST:
        id=request.POST['id']
        payment_mode=request.POST['payment_mode']
        reference_no=request.POST['reference_no']
        FinanceTask.depositEMI.delay(int(id),payment_mode,reference_no)
        return redirect('owner:login_amount')
    context = {
        "menu": menu,
        "emi":emi,
        "date":datetime.now()       
    }
    return render(request, 'owner/finance/emi_deposit.html', context)
def depositLoginAmount(request,id):
    sidebarActive("6")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    customer=Customer.objects.get(id=id)
    form=LoginAmountDeposit(request.POST or None)
    if form.is_valid():
        payment_mode=form.cleaned_data['payment_mode']
        reference_no=form.cleaned_data['reference_no']
        property_ref=form.cleaned_data['property_ref']
        amount=form.cleaned_data['amount']
        desc=form.cleaned_data['description']
        property_ref=Property.objects.get(id=int(property_ref))
        messages.success(request,'Amount has been deposited')
        FinanceTask.depositLoginAmount(payment_mode,reference_no,property_ref,Decimal(amount),desc)
        return redirect('owner:login_amount')
    else:
        if request.POST:
            messages.error(request,'Incorrect form details')
    property_list=Property.objects.filter(customer_ref=customer)
    transactions=Transaction.objects.only('sale_value','balance','property_ref').filter(property_ref__customer_ref_id=id).order_by('date')
    context = {
        "menu": menu,
        "property_list":property_list,
        "form":form,
        "transactions":transactions,
    }
    return render(request, 'owner/finance/login_amount_deposit.html', context)
def payoutList(request):
    sidebarActive("6")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var

    #customer=Customer.objects.get(id=id)
    transactions=AssociateTransaction.objects.all()
    if request.POST:        
        id=request.POST['id']
        try:
            associate=Associate.objects.get(user__username=id)
            messages.success(request,'Associate found')
            return redirect('owner:payout_payments',id=associate.id)
        except Associate.DoesNotExist:
            messages.error(request,'Associate not found')        
    context = {
        "menu": menu,
        "transactions":transactions,
        "date":datetime.now()       
    }
    return render(request, 'owner/finance/payout_list.html', context)
def payoutPayment(request,id):
    sidebarActive("6")
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    earnings_self=SelfEarning.objects.filter(associate_ref_id=id).exclude(is_onhold=True)
    earnings_self_hold=SelfEarning.objects.filter(associate_ref_id=id).filter(is_onhold=True)
    associate_ref=Associate.objects.get(id=id)
    advance_form=AssociateTransactionForm()
    transactions=AssociateTransaction.objects.filter(associate_ref=associate_ref)
    if request.POST:
        payment_mode='A'        
        ernId=request.POST['id']
        page=request.POST['page']
        earning_type=request.POST['earning']
        if earning_type == 'self':
            earningObj=SelfEarning.objects.get(id=int(ernId))
            ern='SERN'
        else:
            #team earning
            earningObj=SelfEarning.objects.get(id=int(ernId))
            ern='TERN'        
        if page == 'pay':
            messages.success(request,'Payment completed')
            FinanceTask.payoutPayments(earningObj,ern,associate_ref,payment_mode)
            return redirect('owner:payout_payments',id=associate_ref.id)        
        """ elif page == 'adjust':
            desc=request.POST['description']
            FinanceTask.adjustPayments(earningObj,ern,associate_ref,payment_mode,reference_no,desc)
            messages.success(request,'Earning Adjusted') """            
    context = {
        "menu": menu,
        "earnings_self":earnings_self,
        "date":datetime.now(),
        "associate":associate_ref,
        "form":advance_form,
        "transactions":transactions,
        "earnings_self_hold":earnings_self_hold       
    }
    return render(request, 'owner/finance/payout_payments.html', context)
def rewards(request):
    sidebarActive("6")
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    transactions=RewardPassbook.objects.all()
    rewards=RewardEarning.objects.all()    
    if request.POST:        
        id=request.POST['id']
        try:
            associate=Associate.objects.get(user__username=id)
            messages.success(request,'Associate found')
            return redirect('owner:redeem_rewards',id=associate.id)
        except Associate.DoesNotExist:
            messages.error(request,'Associate not found')        
    context = {
        "menu": menu,
        "transactions":transactions,
        "date": datetime.now(),
        "rewards":rewards       
    }
    return render(request, 'owner/finance/rewards.html', context)
def redeemRewards(request,id):
    sidebarActive("6")
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    transactions=RewardPassbook.objects.filter(associate_ref_id=id)
    rewards=RewardEarning.objects.select_related('reward_rule').filter(associate_ref_id=id)
    reward_passbook=RewardPassbook.objects.only('sale_amount_balance','reward_balance').filter(associate_ref_id=id).order_by('-createdAt')[:1].aggregate(Sum('sale_amount_balance'))
    reward_rule=RewardsRule.objects.all() 
    associate_ref=Associate.objects.get(id=id)
    form=RedeemForm(request.POST or None)
    if form.is_valid():
        id=form.cleaned_data['id']
        FinanceTask.redeemRewards(associate_ref,int(id))
        messages.success(request,'Reward Redeemed')     
        return redirect('owner:redeem_rewards',id=associate_ref.id)
    context = {
        "menu": menu,   
        "associate":associate_ref,
        "rewards":rewards,
        "transactions":transactions,
        "reward_rule":reward_rule,
        "reward_passbook":reward_passbook,
        "form":form      

    }
    return render(request, 'owner/finance/redeem_rewards.html', context)
def payoutAdvance(request):
    form=AssociateTransactionForm(request.POST or None)
    ret_var=general_access(request,[1,2])
    if ret_var is not None:
        return ret_var
    if form.is_valid():
        associate_ref=form.cleaned_data['associate_ref']
        debit=form.cleaned_data['debit']
        desc=form.cleaned_data['description']
        reference_no=form.cleaned_data['reference_no']
        pay_method=form.cleaned_data['pay_method']
        FinanceTask.advancePayments(associate_ref, pay_method, reference_no,debit,desc)
        messages.success(request,'Advance Payed of '+str(debit))
        return redirect('owner:payout_payments',id=associate_ref.id)  
    return redirect('owner:payout_list')
def otherExpences(request,id):
    sidebarActive("6")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    all_expences=OtherExpences.objects.all().filter(expence_type=id)
    form=OtherExpencesForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request,'Expence Saved')
    elif request.POST:
        messages.error(request,'Invalid Details ')
    context = {
        "menu": menu,   
        "form":form,
        "all_expences":all_expences,
        "expence_type":id   
    }
    return render(request, 'owner/finance/other_expences.html', context)
def releaseEarnings(request,id):
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    earning=SelfEarning.objects.get(id=id)
    earning.is_onhold=False
    earning.save()
    messages.success(request,'Earning Released')
    return redirect('owner:payout_payments',id=earning.associate_ref.id)  

def refunds(request):
    pass
#######################################################################################################################
#Company Policy                        Company Policy                       Company Policy
#######################################################################################################################


def payoutRules(request):
    sidebarActive("9")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    rules = PayoutRule.objects.all()
    
    context = {
        "menu": menu,
        "rules": rules,
    }
    return render(request, 'reports/companyPolicy/payout_rules.html', context)
def payoutRulesCreate(request):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    form=PayoutRuleForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('owner:payout_rules')   
    context = {
        "menu": menu,
        "heading": "Payout Rules Create",
        "form":form,
    }
    return render(request, 'reports/companyPolicy/form.html', context)

def payoutRulesEdit(request,id):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    payout_rule= PayoutRule.objects.get(id=id)
    if request.POST:
        form=PayoutRuleForm(request.POST, instance=payout_rule)
        if form.is_valid():
            form.save()
            return redirect('owner:payout_rules')
    else:
        form=PayoutRuleForm(instance=payout_rule)
    context = {
        "menu": menu,
        "form":form,
        "heading": "Payout Rules Edit",
    }
    return render(request, 'reports/companyPolicy/form.html', context)

def rewardRules(request):
    sidebarActive("9")
    ret_var=general_access(request,[1,2,3,4,5,6])
    if ret_var is not None:
        return ret_var
    rules = RewardsRule.objects.all()
    context = {
        "menu": menu,
        "rules": rules,
    }
    return render(request, 'reports/companyPolicy/reward_rules.html', context)
def rewardRulesCreate(request):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    form=RewardsRuleForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('owner:reward_rules')   
    context = {
        "menu": menu,
        "heading": "Reward Rules Create",
        "form":form,
    }
    return render(request, 'reports/companyPolicy/form.html', context)

def rewardRulesEdit(request,id):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    payout_rule= RewardsRule.objects.get(id=id)
    if request.POST:
        form=RewardsRuleForm(request.POST, instance=payout_rule)
        if form.is_valid():
            form.save()
            return redirect('owner:reward_rules')
    else:
        form=RewardsRuleForm(instance=payout_rule)
    context = {
        "menu": menu,
        "form":form,
        "heading": "Reward Rules Edit",
    }
    return render(request, 'reports/companyPolicy/form.html', context)

def plcRulesCreate(request):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    form=PriceRuleForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('owner:plcRules')   
    context = {
        "menu": menu,
        "heading": "PLC Rules Create",
        "form":form,
    }
    return render(request, 'reports/companyPolicy/form.html', context)

def plcRulesEdit(request,id):
    sidebarActive("9")
    ret_var=general_access(request,[1])
    if ret_var is not None:
        return ret_var
    payout_rule= PriceRule.objects.get(id=id)
    if request.POST:
        form=PriceRuleForm(request.POST, instance=payout_rule)
        if form.is_valid():
            form.save()
            return redirect('owner:plcRules')
    else:
        form=PriceRuleForm(instance=payout_rule)
    context = {
        "menu": menu,
        "form":form,
        "heading": "PLC Rules Edit",
    }
    return render(request, 'reports/companyPolicy/form.html', context)
#######################################################################################################################
#Reports                                           Reports                                   Reports
#######################################################################################################################

def salesReport(request):
    sidebarActive("7")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    sales=Sale.objects.only('createdAt','amount').filter(status=True).order_by('-createdAt')
    context = {
        "menu": menu,
        "sales":sales,
    }
    return render(request, 'reports/reports/sales_report.html', context)

def buisnessReport(request):
    sidebarActive("7")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    income=Transaction.objects.only('credit','debit','date').all().order_by('-date')
    payments=AssociateTransaction.objects.only('credit','debit','date').all().order_by('-date')
    rewards=RewardPassbook.objects.only('createdAt','debit_amount').all().order_by('-createdAt')
    expences=OtherExpences.objects.only('date','amount').all().order_by('-date')    
    context = {
        "menu": menu,
        "income":income,
        "payments":payments,
        "rewards":rewards,
        "expences":expences
    }
    return render(request, 'reports/reports/buisness_report.html', context)

def emiReport(request):
    sidebarActive("7")
    ret_var=general_access(request,[1,2,3])
    if ret_var is not None:
        return ret_var
    emi=EMI.objects.only('pay_day','emi').order_by('-pay_day')
    context = {
        "menu": menu,
        "emi_list":emi
    }
    return render(request, 'reports/reports/emi_report.html', context)