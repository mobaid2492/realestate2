from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth
from .forms import ContactForm,CarrerForm
from .models import Contact,Carrer
from projects.models import Project,Section,Property,PriceRule
from config.models import Company,Spotlight,Office,BankDetail,MediaMentions,Gallery,Document
#from .forms import CustomerForm

def getCompany():
  company_detail=Company.objects.all()
  return company_detail
def siteActive():
  ret=None
  for company in Company.objects.all()[:1]:
    if company.is_active == False:
      ret=redirect('webapp:maintanance')
    else:
      ret=None
  return ret
def index(request):
  project_list=Project.objects.all()
  spotlights=Spotlight.objects.all()
  context={
            "details":getCompany(),            
            "project_list":project_list,
            "spotlights":spotlights,
                 
  }
  return render(request ,'webapp/index.html',context)

def careers(request):
  form=CarrerForm(request.POST or None)
  if form.is_valid():
    form.save()  
  context={
           "details":getCompany(),
           "form":form                    
  }
  return render(request ,'webapp/careers.html',context)

def contactUs(request):
  offices=Office.objects.all()
  form=ContactForm(request.POST or None)
  if form.is_valid():
    form.save()  
  context={
           "offices":offices,
           "details":getCompany(),
           "form":form
                                 
  }
  return render(request ,'webapp/contactus.html',context) 

def maintance(request):
  return render(request ,'webapp/maintanance_mode.html')

def aboutUs(request):
  bank_accounts=BankDetail.objects.all() 
  media=MediaMentions.objects.all()
  gallery=Gallery.objects.all()
  documents=Document.objects.all()
  context={
           "details":getCompany(),
           "bank_accounts":bank_accounts, 
           "media":media,
           "gallery":gallery,
           "documents":documents,                    
  }
  return render(request ,'webapp/aboutus.html',context) 