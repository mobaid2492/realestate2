from django.db import models

# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=50)
    email=models.EmailField(blank=True, null=True)
    phone=models.BigIntegerField(blank=True, null=True)
    message=models.TextField(blank=True, null=True)

class Carrer(models.Model):
    name = models.CharField(max_length=50)
    email=models.EmailField(blank=True, null=True)
    phone=models.BigIntegerField(blank=True, null=True)
    message=models.TextField(blank=True, null=True)
