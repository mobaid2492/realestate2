from django import forms
from .models import Contact,Carrer
from django.contrib.auth.models import User
class ContactForm(forms.ModelForm):
    class Meta:
        model= Contact
        fields= '__all__'
class CarrerForm(forms.ModelForm):
    class Meta:
        model= Carrer
        fields= '__all__'
class UserLogin(forms.ModelForm):
    class Meta:
        model=User
        fields=  ['username', 'password']
