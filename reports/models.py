from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from projects.models import Project,Property,Transaction,EMI
from associate.models import Associate


class Sale(models.Model):
    amount=models.IntegerField()
    property_ref=models.ForeignKey(Property,on_delete=models.SET_NULL,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    status=models.BooleanField(default=False)
    createdAt= models.DateTimeField(auto_now_add=True)
    #status is to check whether it has been calncelled or not
    #year = property(_get_year) #EDIT

#use celery to generate monthly sale report summary of sale table
class MonthlySales(models.Model):
    MONTH = (             
        ('JAN', 'January'),
        ('FEB', 'February'),
        ('MAR', 'March'),
        ('APR', 'April'),
        ('MAY', 'May'),
        ('JUN', 'June'),
        ('JUL', 'July'),
        ('AUG', 'August'),
        ('SEP', 'September'),
        ('OCT', 'October'),
        ('NOV', 'November'),
        ('DEC', 'December'),        
    )
    amount=models.IntegerField()
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    sale_month=models.IntegerField()
    sale_year=models.IntegerField()
    status=models.BooleanField(default=False)
    createdAt= models.DateTimeField(auto_now_add=True)
    last_updated= models.DateTimeField(auto_now=True)

#amount needs to calculated using payout table
#this table need to updated when customer pays
class PayoutRule(models.Model):
    sale_start_amount=models.IntegerField()
    sale_end_amount=models.IntegerField()
    payout=models.IntegerField()
    reward=models.IntegerField()
    is_monthly=models.BooleanField(default=False)

#after evrery calculations this table will be updated if associate fulfils rule. till 
class PayoutLifetime(models.Model):
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.SET_NULL,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)

class PayoutMonthly(models.Model):
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.SET_NULL,blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)

# to show the earnings add absolute percentage 
class SelfEarning(models.Model):
    amount=models.DecimalField(max_digits=19, decimal_places=2)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    status=models.BooleanField(default=False)
    payout_rule=models.ForeignKey(PayoutRule,on_delete=models.SET_NULL,blank=True, null=True)
    payout_rule_value=models.IntegerField(blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)
    cus_tx=models.ForeignKey(Transaction,on_delete=models.SET_NULL,blank=True, null=True)
    is_onhold=models.BooleanField(default=False)
#status shows payment staus of this earning
class TeamEarning(models.Model):
    amount=models.DecimalField(max_digits=19, decimal_places=2)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    status=models.BooleanField(default=False)
    payout=models.DecimalField(max_digits=5, decimal_places=2)
    createdAt= models.DateTimeField(auto_now_add=True)
    cus_tx=models.ForeignKey(Transaction,on_delete=models.SET_NULL,blank=True, null=True)
    is_onhold=models.BooleanField(default=False)
#add team earning rule
#for amount recived payment by the associate
class AssociateTransaction(models.Model):
    METHOD = (             
        ('C', 'Cash Payment'),
        ('Q', 'Cheque'),
        ('N', 'NEFT'),
        ('D', 'Bank Draft'),
        ('A', 'Added balance'),
                
    )
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL,blank=True, null=True)
    pay_method=models.CharField(max_length=1, choices=METHOD)
    date=models.DateTimeField(blank=True,auto_now=False, auto_now_add=True)
    reference_no=models.CharField(blank=True,null=True, max_length=50)
    credit=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    debit=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    balance=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    description=models.TextField(blank=True,null=True)

class DeductionsRule(models.Model):
    value=models.IntegerField()
    reason=models.CharField(max_length=50)
    #add fields credit, debit(with name advance) balance
class OtherExpences(models.Model):
    METHOD = (             
        ('C', 'Cash Payment'),
        ('Q', 'Cheque'),
        ('N', 'NEFT'),
        ('D', 'Bank Draft'),
        ('A', 'Added balance'),                
    )
    EXPENCE_TYPE_CHOICES = (
      (1, 'Marketing'),
      (2, 'Office Expence'),
      (3, 'Employee Salary'),
      (4, 'Tour'),
      (5, 'Visit'),
      (6, 'Other Expences'),      
    )
    expence_type = models.PositiveSmallIntegerField(choices=EXPENCE_TYPE_CHOICES,blank=True, null=True)
    pay_method=models.CharField(max_length=1, choices=METHOD)
    date=models.DateTimeField(blank=True,auto_now=False, auto_now_add=True)
    reference_no=models.CharField(blank=True,null=True, max_length=50)
    amount=models.DecimalField( max_digits=20, decimal_places=2,blank=True,null=True)
    description=models.TextField(blank=True,null=True)
    #add fields credit, debit(with name advance) balance
      
#in lakhs
class RewardsRule(models.Model):
    sale_amount=models.IntegerField()
    reward=models.CharField(max_length=50)
class RewardEarning(models.Model):     
    reward_rule=models.ForeignKey(RewardsRule,on_delete=models.SET_NULL, blank=True, null=True)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL, blank=True, null=True)
    createdAt= models.DateTimeField(auto_now_add=True)
    status=models.BooleanField(default=True)     

class RewardPassbook(models.Model):
    createdAt= models.DateTimeField(auto_now_add=True)
    description=models.CharField(max_length=100)
    sale_amount_balance=models.DecimalField(max_digits=10, decimal_places=2)
    associate_ref=models.ForeignKey(Associate,on_delete=models.SET_NULL, blank=True, null=True)
    reward_value=models.IntegerField(default=5)
    debit_amount=models.DecimalField(max_digits=10, decimal_places=2)
    credit_amount=models.DecimalField(max_digits=10, decimal_places=2)
    reward_balance=models.DecimalField(max_digits=10, decimal_places=2)

 
