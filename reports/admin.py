from django.contrib import admin
from .models import AssociateTransaction, Sale,PayoutRule,PayoutMonthly,PayoutLifetime, SelfEarning, TeamEarning

admin.site.register(Sale)
admin.site.register(PayoutRule)
admin.site.register(PayoutLifetime)
admin.site.register(PayoutMonthly)
admin.site.register(SelfEarning)
admin.site.register(TeamEarning)
admin.site.register(AssociateTransaction)
# Register your models here.
