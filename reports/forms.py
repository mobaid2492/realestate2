from django import forms
from .models import Sale, AssociateTransaction,OtherExpences,RewardsRule,PayoutRule
from projects.models import PriceRule

class SaleForm(forms.ModelForm):
    class Meta:
        model = Sale
        fields = ['amount',
                  'property_ref',
                  'associate_ref',
                  'status'
                  ]


class BookingForm(forms.Form):
    total_amount = forms.DecimalField(
        max_digits=6, decimal_places=2, required=False)
    booking_amount = forms.DecimalField(
        max_digits=6, decimal_places=2, required=False)
    payment_mode = forms.CharField(max_length=1, required=True)
    emi_plan = forms.IntegerField(required=True)
    reference_no = forms.CharField(max_length=100, required=True)
class RedeemForm(forms.Form):
    id=forms.IntegerField(required=True)


class AssociateTransactionForm(forms.ModelForm):

    class Meta:
        model = AssociateTransaction
        fields = ['associate_ref',
                  'pay_method',
                  'reference_no',
                  'debit',
                  'description'
                  ]


class OtherExpencesForm(forms.ModelForm):    
    class Meta:
        model = OtherExpences
        fields = ("__all__")

class RewardsRuleForm(forms.ModelForm):
    class Meta:
        model = RewardsRule
        fields = ("__all__")

class PriceRuleForm(forms.ModelForm):
    class Meta:
        model = PriceRule
        fields = ("__all__")

class PayoutRuleForm(forms.ModelForm):
    class Meta:
        model = PayoutRule
        fields = ("__all__")
