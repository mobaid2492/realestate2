import string
from django.db.models import Sum, Count, Max
from user.models import User
from django.utils.crypto import get_random_string
from django.utils.timezone import timezone
from celery import shared_task
from reports.models import Sale, AssociateTransaction, MonthlySales, PayoutRule, PayoutLifetime, PayoutMonthly, SelfEarning, RewardEarning, RewardPassbook, RewardsRule
from associate.models import Associate
from projects.models import Transaction, EMI, TempBooking, Property
from .calculations import slab_lifetime, slab_monthly
from decimal import Decimal
from datetime import date, timedelta,datetime
from celery.task.schedules import crontab
from celery.decorators import periodic_task

class PayoutTask():

    @shared_task
    def create_random_user_accounts(total):
        for i in range(total):
            username = 'user_{}'.format(
                get_random_string(10, string.ascii_letters))
            email = '{}@example.com'.format(username)
            password = get_random_string(50)
            User.objects.create_user(
                username=username, email=email, password=password)
        return '{} random users created with success!'.format(total)

    def lifetime_slab(sales):
        rules= PayoutRule.objects.filter(is_monthly=False)            
        num=sales/100000
        pay=0
        fact=0
        for rule in rules:        
            if num>rule.sale_start_amount and num<=rule.sale_end_amount:       
                num-=rule.sale_end_amount
                fact=rule.payout
            elif num>rule.sale_end_amount:
                num=num-rule.sale_end_amount        
                fact=rule.payout
        print(fact)
        return fact
        
    
    def monthly_slab(sales):
        rules= PayoutRule.objects.filter(is_monthly=True)                    
        num=Decimal(sales/100000)
        num-=45
        pay=0
        fact=0
        for rule in rules:        
            if num>rule.sale_start_amount and num<=rule.sale_end_amount:       
                num-=rule.sale_end_amount
                fact=rule.payout
            elif num>rule.sale_end_amount:
                num=num-rule.sale_end_amount        
                fact=rule.payout      
        print(fact)
        return fact

    def generate_monthly_sales_report():
        sales_report = Sale.objects.only(
            'associate_ref', 'createdAt', 'amount').order_by('createdAt').all()
        associate_list = Associate.objects.all()
        year = 0
        month = 0
        amount = 0
        for item2 in associate_list:
            for item in sales_report:
                if(item.createdAt.year == year) and (item.createdAt.month == month) and (item.associate_ref_id == item2.id):
                    amount += item.amount
                else:
                    if amount > 0 and item.createdAt.month > month:
                        monthly_sale = MonthlySales(
                            amount=amount, associate_ref_id=item2.id, sale_month=month, sale_year=year)
                        monthly_sale.save()
                        amount = 0
                    if item.createdAt.year > year:
                        year = item.createdAt.year
                        month = 1
                    if item.createdAt.month > month:
                        month = item.createdAt.month

            year = 0
            month = 0
            amount = 0
        return month
    # send data only when lifetime sale is above 45 lakhs

    # get last entry

    @shared_task
    def update_lifetime_payout(associate):
        associate=Associate.objects.get(id=associate.id)
        var = False
        print("update_lifetime_payout")
        lifetime_sales = Sale.objects.filter(
            associate_ref=associate).aggregate(Sum('amount'))
        print(lifetime_sales)
        cal_payout=0
        if lifetime_sales['amount__sum'] is not None:
            cal_payout = PayoutTask.lifetime_slab(lifetime_sales['amount__sum'])
        payout_self_lst = PayoutLifetime.objects.filter(
                associate_ref=associate).order_by('-createdAt')[:1]
        if payout_self_lst and cal_payout > 0:
            for payout_self in payout_self_lst:
                print("ONE")                
                if payout_self is not None:
                    if(payout_self.payout_rule.payout < cal_payout):
                        get_payout_rule = PayoutRule.objects.get(payout=cal_payout)
                        payout_update = PayoutLifetime(
                               payout_rule=get_payout_rule, associate_ref=associate)
                        payout_update.save()
                        var = True
        else:
            print("zero")
            if cal_payout > 0:     
                get_payout_rule = PayoutRule.objects.get(payout=cal_payout)
                payout_update = PayoutLifetime(
                        payout_rule=get_payout_rule, associate_ref=associate)
                payout_update.save()
        if lifetime_sales['amount__sum']:
            if lifetime_sales['amount__sum'] > 4500000:
                print("45") 
                PayoutTask.update_monthly_payout(associate)

    @shared_task
    def update_monthly_payout(associate):
        monthly_sales = Sale.objects.filter(
            associate_ref=associate).filter(createdAt__month=datetime.now().month).aggregate(Sum('amount'))
        # sale this month
        cal_payout=0
        if monthly_sales['amount__sum'] is not None:
            cal_payout= PayoutTask.monthly_slab(monthly_sales['amount__sum'])
            print("cal_payout"+str(cal_payout))              
          
        payout_self_lst = PayoutMonthly.objects.filter(
                associate_ref=associate).order_by('-createdAt')[:1]
        if payout_self_lst:            
            for payout_self in payout_self_lst:                
                if payout_self is not None:
                    if(payout_self.payout_rule.payout < cal_payout):
                        get_payout_rule = PayoutRule.objects.get(payout=cal_payout)
                        payout_update = PayoutMonthly(
                                payout_rule=get_payout_rule, associate_ref=associate)
                        payout_update.save()
                        var = True
        else:
            print("cal_payout"+str(monthly_sales['amount__sum']))
            if cal_payout > 0:
                get_payout_rule = PayoutRule.objects.get(payout=cal_payout)
                payout_update = PayoutMonthly(
                            payout_rule=get_payout_rule, associate_ref=associate)
                payout_update.save()


    def team_earnings(parameter_list):
        pass


    def calculate_earnings(parameter_list):
        pass


class AssociateTask():
    def create_user_accounts(email, first_name, last_name, photo, user_type, password):
        username = 'CBDA{}'.format(get_random_string(4, '0123456789'))
        user = User.objects.create_user(username=username, email=email, password=password,
                                        photo=photo, first_name=first_name, last_name=last_name, user_type=user_type)
        return user

    @shared_task
    def get_downlineTree(id):
        associate = Associate.objects.get(id=id)
        family = associate.get_family()
        for member in family:
            print(member.name+" "+str(member.id))

    @shared_task
    def createTree(id):
        associate = Associate.objects.get(id=id)
        for i in range(10):
            phone = '{}'.format(get_random_string(10, '0123456789'))
            email = '{}@gmail.com'.format(
                get_random_string(4, 'abcdefghifklmnpqrstuv'))
            ass1 = Associate.objects.create(name="name", parent=associate,
                                            contact_no=phone,  email=email,
                                            gender='M')
            ass2 = Associate.objects.create(name="name", parent=associate,
                                            contact_no=phone,  email=email,
                                            gender='M')
            associate = ass1

    @shared_task
    def getDownlineSalesTree(id):
        class MyClass:
            def __init__(self, sale, balance):
                self.sale = sale
                self.balance = balance
            sale = Sale()
            balance = 0

        associate = Associate.objects.get(id=id)
        family = associate.get_descendants(include_self=True)#associate.get_family()
        sales = Sale.objects.all()
        sales_list = []
        for member in family:
            for entry in sales:
                # print(entry)
                if entry.associate_ref == member:
                    property_tx = Transaction.objects.only('balance', 'sale_value').filter(
                        property_ref__customer_ref__associate_ref=member).order_by('-date')[:1]
                    entryobj = MyClass(entry, balance=property_tx[0].balance)
                    sales_list.append(entryobj)


        """ for entry in sales:
            if entry.associate_ref == associate:
                    property_tx = Transaction.objects.only('balance', 'sale_value').filter(
                        property_ref__customer_ref__associate_ref=associate).order_by('-date')[:1]
                    entryobj = MyClass(entry, balance=property_tx[0].balance)
                    sales_list.append(entryobj) """                    
        return sales_list


class FinanceTask():
    @shared_task
    def depositEMI(id, payment_mode, reference_no):
        emi = EMI.objects.get(id=id)
        emi.status = True
        emi.save()
        tx = BookingTask.addPropertyTransaction(
            payment_mode, emi.property_ref, reference_no, 0,emi.emi,"Paid Emi- EMID-"+str(emi.id))
        BookingTask.generateAssociateEarnings(
            emi.emi, emi.property_ref.customer_ref.associate_ref, tx)
        BookingTask.generateAssociateRewardsPassbook(
            emi.emi, emi.property_ref.customer_ref.associate_ref, "Reward for paid Emi ")

    def depositLoginAmount(payment_mode, reference_no, property_ref, amount, desc):
        associate_ref = property_ref.customer_ref.associate_ref
        tx = BookingTask.addPropertyTransaction(
            payment_mode, property_ref, reference_no, 0,amount, desc)
        BookingTask.generateAssociateEarnings(amount, associate_ref, tx)
        BookingTask.generateAssociateRewardsPassbook(
            amount, associate_ref, desc)

    def payoutPayments(earning_ref, earning_type, associate_ref, pay_method):
        earning_ref.status = True
        earning_ref.save()
        transactions = AssociateTransaction.objects.filter(
            associate_ref=associate_ref)[:1]
        balance = 0
        for entry in transactions:
            balance += entry.balance
        payment = AssociateTransaction(associate_ref=associate_ref,
                                       pay_method=pay_method, reference_no=earning_type+str(earning_ref.id),
                                       credit=earning_ref.amount*Decimal(0.95),
                                       debit=0,
                                       balance=balance +
                                       earning_ref.amount*Decimal(0.95),
                                       description="Earning Added")
        payment.save()
    def advancePayments(associate_ref, pay_method, reference_no,advance,desc):
        
        transactions = AssociateTransaction.objects.filter(
            associate_ref=associate_ref)[:1]
        balance = 0
        for entry in transactions:
            balance += entry.balance
        payment = AssociateTransaction(associate_ref=associate_ref,
                                       pay_method=pay_method, reference_no=reference_no,
                                       credit=0,
                                       debit=advance,
                                       balance=balance -
                                       advance,
                                       description="Withdrawl as: "+desc)
        payment.save()

    def adjustPayments(earning_ref, earning_type, associate_ref, pay_method, reference_no, desc):
        earning_ref.status = True
        earning_ref.save()
        payment = AssociateTransaction(associate_ref=associate_ref,
                                       pay_method=pay_method, reference_no=reference_no,
                                       amount=0,
                                       description=desc)
        payment.save()

    def encashRewards(associate_ref):
        rewards_rules = RewardsRule.objects.only('sale_amount').all()
        pos = 0
        reward_passbook = RewardPassbook.objects.only('sale_amount_balance', 'reward_balance').filter(
            associate_ref=associate_ref).order_by('-createdAt')[:1]
        entry_id = 0
        value = 0
        if reward_passbook.exists():
            for entry in reward_passbook:
                entry_id = entry.id
                value = entry.sale_amount_balance/100000
        if rewards_rules.exists():
            for rule in rewards_rules:
                if rule.sale_amount < value:
                    pos = rule.id
        if pos > 0:
            new_reward = RewardEarning(
                reward_rule_id=pos, associate_ref=associate_ref, status=True)
            # new_reward.save()
            reward_obj = RewardsRule.objects.get(id=pos)
            old_entry = RewardPassbook.objects.get(id=entry_id)
            new_sale_amount = old_entry.sale_amount_balance - \
                (reward_obj.sale_amount*100000)
            new_debit_amount = reward_obj.sale_amount*100000*old_entry.reward_value/100
            new_reward_balance = old_entry.reward_balance - \
                Decimal(new_debit_amount)
            new_entry = RewardPassbook(sale_amount_balance=new_sale_amount,
                                       associate_ref=associate_ref,
                                       reward_value=old_entry.reward_value,
                                       credit_amount=0,
                                       debit_amount=new_debit_amount,
                                       reward_balance=Decimal(
                                           new_reward_balance),
                                       description=old_entry.description)
            new_entry.save()
            new_earning = SelfEarning(amount=Decimal(new_reward_balance),
                                      associate_ref=associate_ref,
                                      payout_rule_value=5)
            new_earning.save()

    def redeemRewards(associate_ref, id):
        rewards_rule = RewardsRule.objects.only('sale_amount').get(id=id)
        pos = 0
        reward_passbook = RewardPassbook.objects.only('sale_amount_balance', 'reward_balance').filter(
            associate_ref=associate_ref).order_by('-createdAt')[:1]
        entry_id = 0
        value = 0
        if reward_passbook.exists():
            for entry in reward_passbook:
                entry_id = entry.id
                value = entry.sale_amount_balance/100000

        new_reward = RewardEarning(
            reward_rule=rewards_rule, associate_ref=associate_ref, status=True)
        new_reward.save()
        old_entry = RewardPassbook.objects.get(id=entry_id)

        new_sale_amount = old_entry.sale_amount_balance - \
            (rewards_rule.sale_amount*100000)
        new_debit_amount = rewards_rule.sale_amount*100000*old_entry.reward_value/100
        new_reward_balance = old_entry.reward_balance-Decimal(new_debit_amount)

        new_entry = RewardPassbook(sale_amount_balance=new_sale_amount,
                                   associate_ref=associate_ref,
                                   reward_value=old_entry.reward_value,
                                   credit_amount=0,
                                   debit_amount=new_debit_amount,
                                   reward_balance=Decimal(new_reward_balance),
                                   description=old_entry.description)
        new_entry.save()


class BookingTask():
    def completeBooking(payment_mode, property_ref, reference_no, booking_amount, total_amount, emi_plan, associate_ref):
        PayoutTask.update_lifetime_payout(associate_ref)
        tx = BookingTask.addPropertyTransaction(
            payment_mode, property_ref, reference_no, total_amount, booking_amount, "Paid for the booking")
        BookingTask.bookProperty(property_ref,emi_plan)
        BookingTask.generateEmi(
            property_ref, total_amount, booking_amount, emi_plan)
        BookingTask.generateAssociateEarnings(
            booking_amount, associate_ref, tx)
        BookingTask.generateAssociateRewardsPassbook(
            booking_amount, associate_ref, "Reward for the new booking")

    def bookProperty(property_ref,payment_plan):
        property_ref.status_code = 'B'
        property_ref.emi_plan=payment_plan
        property_ref.save()

    def addPropertyTransaction(payment_mode, property_ref, reference_no, total_amount, booking_amount, desc):
        transactions = Transaction.objects.filter(
            property_ref=property_ref).order_by('-date')[:1]
        balance = 0
        print(balance)
        sale_value=total_amount
        for entry in transactions:
            print(entry.balance)
            balance += entry.balance
            if sale_value<1:
                sale_value=entry.sale_value
            print(balance)
        balance+=booking_amount
        print(balance)
        tx = Transaction(pay_method=payment_mode,
                         sale_value=sale_value,
                         property_ref=property_ref,
                         reference_no=reference_no,
                         credit=booking_amount,
                         debit=0,
                         balance=balance,
                         description=desc)
        tx.save()
        return tx

    def generateEmi(property_ref, total_amount, booking_amount, emi_plan):
        opening_bal = Decimal(total_amount)-Decimal(booking_amount)
        date_now = (date.today().day) % 28 or 28
        next_month = (date.today().month + 1) % 12 or 12
        current_year = date.today().year
        if int(emi_plan) > 0:
            months = int(emi_plan)*12
            emi_value = opening_bal/months
            for i in range(months):
                new_emi = EMI(property_ref=property_ref,
                              opening_balance=opening_bal,
                              emi=emi_value,
                              intrest_paid=0,
                              pay_day=str(current_year)+'-' +
                              str(next_month)+'-'+str(date_now),
                              closing_balance=opening_bal-emi_value)
                new_emi.save()
                if next_month == 12:
                    current_year += 1
                opening_bal -= emi_value
                next_month = (next_month+1) % 12 or 12
        else:
            months = 1

    def generateAssociateEarnings(booking_amount, associate_ref, tx):
        PayoutTask.update_lifetime_payout(associate_ref)
        payout_list = PayoutLifetime.objects.select_related('payout_rule').only(
            'payout_rule').filter(associate_ref=associate_ref).order_by('-createdAt')[:1]
        payout_rule=None
        is_onhold=False
        if associate_ref.is_payment_on_hold:
            is_onhold=True
        for payoutObj in payout_list:
            payout_rule = payoutObj.payout_rule
        if payout_rule is not None:
            earning_amount = Decimal(booking_amount)*payout_rule.payout/100
            new_earning = SelfEarning(amount=earning_amount,
                                    associate_ref=associate_ref,
                                    payout_rule=payout_rule,
                                    payout_rule_value=payout_rule.payout,
                                    cus_tx=tx,
                                    is_onhold=is_onhold)
            new_earning.save()

    def generateAssociateRewardsPassbook(booking_amount, associate_ref, desc):
        reward_passbook = RewardPassbook.objects.only('sale_amount_balance', 'reward_balance').filter(
            associate_ref=associate_ref).order_by('-createdAt')[:1]
        reward_rule_value = 5
        entry_id = 0
        if reward_passbook.exists():
            for entry in reward_passbook:
                entry_id = entry.id
            if entry_id > 0:
                reward_entry = RewardPassbook.objects.get(id=entry_id)
                new_reward_value = booking_amount*reward_rule_value/100
                new_sale_balance = reward_entry.sale_amount_balance+booking_amount
                new_reward_balance = reward_entry.reward_balance+new_reward_value
                new_entry = RewardPassbook(sale_amount_balance=new_sale_balance,
                                           associate_ref=associate_ref,
                                           reward_value=reward_rule_value,
                                           credit_amount=new_reward_value,
                                           debit_amount=0,
                                           reward_balance=new_reward_balance,
                                           description=desc)
                new_entry.save()

        else:
            new_reward_value = booking_amount*reward_rule_value/100
            new_entry = RewardPassbook(sale_amount_balance=booking_amount,
                                       associate_ref=associate_ref,
                                       reward_value=reward_rule_value,
                                       credit_amount=new_reward_value,
                                       debit_amount=0,
                                       reward_balance=new_reward_value,
                                       description=desc)
            new_entry.save()
            entry_id = new_entry.id
        # BookingTask.generateAssociateRewardsEarnings(associate_ref)


class CustomerTask():
   # @periodic_task(run_every=crontab(crontab(minute="34")))
    @shared_task
    def customerHoldBooking(id):
        temp_bookings = TempBooking.objects.filter(
            property_ref_id=id, status=True)
        for entry in temp_bookings:
            entry.status = False
            entry.save()
        properties = Property.objects.get(id=id)
        properties.customer_ref = None
        print("working")
        properties.status_code = 'V'
        properties.save()
